// Joe Kimlinger
#include "gfxnew.h"
#include "Board.h"
#include <iostream>
using namespace std;


Board::Board(){
  for (int i = 0; i < 6; i++){
    for (int j = 0; j < 7; j++){
      board[i][j] = 'o';	// o is empty board space
    }
  }
}

Board::~Board(){}

// Drop piece into user selected column
//   returns 0 for unplaced piece, 1 for placed, and 2 for win
int Board::drop(int col, char c){
  bool placed = false;
  int i = 5;
  // Start in row 5, bottom of the board, and go upwards until there
  //   is an empty space
  while (i >= 0 && !placed){
    if (board[i][col] == 'o'){
      board[i][col] = c;
      placed = true;
    }
    else i--;
  }
  // Check to see if placement makes 4 in a row
  if (placed){
    if (check(i, col, c, 4)) return 2;
    else return 1;
  }
  return 0;
}

// Drop based on ai of difficulty 1 or 2
//   return 0 for unplaced piece, 1 for placed piece, 2 for win
int Board::cpuDrop(int diff){
  int i;
  // Check each available column to see if placing a piece there would reult in
  //   4 consecutive pieces, 3, or 2
  for (int consec = 4; consec >= 2; consec--){
    // Iterate through each row
    for (int col = 0; col < 7; col++){
      i = 5;
      while (i >= 0){
        if (board[i][col] == 'o'){
          if (check(i, col, 'b', consec)){
            // If placing piece there makes 4 in a row, place it right away
            if (consec == 4){
              board[i][col] = 'b';
              return 2;
            }
            // if harder difficulty, ensure that placing piece doesnt give user
            //   4 in a row
            else if (diff == 2){
              if (i == 0 || !check(i - 1, col, 'r', 4)){
                board[i][col] = 'b';
                return 1;
              }
              else break;
            }
            else {
              board[i][col] = 'b';
              return 1;
            }
          }
          else break;
        }
        else i--; 
      }
    }
    // Check each open spot to attempt to block the users consecutive peices
    for (int col = 0; col < 7; col++){
      i = 5;
      while (i >= 0){
        if (board[i][col] == 'o'){
          if (check(i, col, 'r', consec)){
            if (consec == 4){
              board[i][col] = 'b';
              return 1;
            }
            // check if placing piece would cause 4 in a row
            else if (diff == 2){
              if (i == 0 || !check(i - 1, col, 'r', 4)){
                board[i][col] = 'b';
                return 1;
              }
              else break;
            }
            else {
              board[i][col] = 'b';
              return 1;
            }
          }
          else break;
        }
        else i--;
      }
    }
  }
  return 0;
}

// Check if placing a piece at [row][col] gives you num c's in a row
//   return true if yes, false if no
bool Board::check(int row, int col, char c, int num){
  int i = 1, j = 1, s = 1;
  int count = 0;

  // Diagonal - slope
  //   check up and left
  while (i <= num - 1){
    if (row + i > 5 || col - j < 0) break;
    if (board[row + i][col - j] == c) count++;
    else break;
    i++, j++;
  }
  // Check down and right based on how far we went up and left
  j = 1;
  while (s <= num - i){
    if (row - s < 0 || col + j > 6) break;
    if (board[row - s][col + j] == c) count++;
    else break;
    s++, j++;
  }
  if (count >= num - 1) return true;

  // Diagonal + slope
  //   check down and left
  i = 1, j = 1, s = 1, count = 0;
  while (i <= num - 1){
    if (row - i > 5 || col - j < 0) break;
    if (board[row - i][col - j] == c) count++;
    else break;
    i++, j++;
  }
  // up and right
  j = 1;
  while (s <= num - i){
    if (row + s < 0 || col + j > 6) break;
    if (board[row + s][col + j] == c) count++;
    else break;
    s++, j++;
  }
  if (count >= num - 1) return true;

  // Horizontal
  //   check to left
  j = 1, s = 1, count = 0;
  while (j <= num - 1){
    if (row > 5 || col - j < 0) break;
    if (board[row][col - j] == c) count++;
    else break;
    j++;
  }
  // Check right
  int d = num - j;
  j = 1;
  while (j <= d){
    if (row < 0 || col + j > 6) break;
    if (board[row][col + j] == c) count++;
    else break;
    j++;
  }
  if (count >= num - 1) return true;

  // Vertical
  //   only check if there is enough space between current spot and bottom of
  //   board to have num in a row 
  i = 1;
  count = 0;
  if (row < num - 1){
    while (i <= num - 1){
      if (board[row + i][col] == c) count++;
      i++;
    }
  }
  if (count >= num - 1) return true;
  
  return false;
}

void Board::display(int w, int h){

  gfx_color(255, 255, 0);
  gfx_fill_rectangle(0, 0, w, h);
  int dw = w/7;
  int dh = h/6;

  gfx_flush();

  // Iterate through board and display a circle that is either red or black
  //   depending on current char
  for (int i = 0; i < 6; i++){
    for (int j = 0; j < 7; j++){
      if (board[i][j] == 'b') gfx_color(0, 0, 0);
      else if (board[i][j] == 'r') gfx_color(255, 0, 0);
      else gfx_color(255, 255, 255);
      gfx_fill_circle(dw*(j+.5), dh*(i+.5), dw/2 - 5);
    }
  }
  gfx_flush();
}

bool Board::full(){
  for (int i = 0; i < 6; i++){
    for (int j = 0; j < 7; j++){
      if (board[i][j] == 'o') return false;
    }
  }
  return true;
}

void Board::clear(){
  for (int i = 0; i < 6; i ++){
    for (int j = 0; j < 7; j++){
      board[i][j] = 'o';
    }
  }
}
