// Joe Kimlinger
#include "gfxnew.h"
#include "Board.h"
#include <iostream>
using namespace std;


Board::Board(){
  for (int i = 0; i < 6; i++){
    for (int j = 0; j < 7; j++){
      board[i][j] = 'o';
    }
  }
}

Board::~Board(){}

int Board::drop(int col, char c){
  bool placed = false;
  int i = 5;
  while (i >= 0 && !placed){
    if (board[i][col] == 'o'){
      board[i][col] = c;
      placed = true;
    }
    else i--;
  }
  if (placed){
    if (check(i, col, c, 4)) return 2;
    else return 1;
  }
  return 0;
}

int Board::cpuDrop(){
  bool placed = false;
  int i;
  for (int consec = 4; consec >= 2; consec--){
    for (int col = 0; col < 7; col++){
      i = 5;
      while (i >= 0 && !placed){
        if (board[i][col] == 'o'){
          if (check(i, col, 'b', consec)){
            board[i][col] = 'b';
            if (consec == 4){
              placed = true;
              return 2;
            }
            else{
              placed = true;
              return 1;
            }
          } 
          else if (check(i, col, 'r', consec)){
            board[i][col] = 'b';
            return 1;
          }
          else break;
        }
        else i--;
      }
    }
  }
  return 0;
}


bool Board::check(int row, int col, char c, int num){
  int i = 1, j = 1, s = 1;
  int count = 0;
 // int spaces = 0;

  // Diagonal + slope
  while (i <= num - 1){
    if (row + i > 5 || col - j < 0) break;
    if (board[row + i][col - j] == c) count++;
    else break;
    i++, j++;
  }
  j = 1;
  while (s <= num - i){
    if (row - s < 0 || col + j > 6) break;
    if (board[row - s][col + j] == c) count++;
    else break;
    s++, j++;
  }
  if (count >= num - 1) return true;
  //cout << "+ " << count << endl;

  // Diagonal - slope
  i = 1, j = 1, s = 1, count = 0;
  while (i <= num - 1){
    if (row + i > 5 || col - j < 0) break;
    if (board[row + i][col - j] == c) count++;
    else break;
    i++, j++;
  }
  j = 1;
  while (s <= num - i){
    if (row - s < 0 || col + j > 6) break;
    if (board[row - s][col + j] == c) count++;
    else break;
    s++, j++;
  }
  if (count >= num - 1) return true;
  //cout << "- " << count << endl;

  // Horizontal
  j = 1, s = 1, count = 0;
  while (j <= num - 1){
    if (row > 5 || col - j < 0) break;
    if (board[row][col - j] == c) count++;
    else break;
    j++;
  }
  int d = num - j;
  j = 1;
  while (j <= d){
    if (row < 0 || col + j > 6) break;
    if (board[row][col + j] == c) count++;
    else break;
    j++;
  }
  if (count >= num - 1) return true;
  //cout << "H " << count << endl;

  // Vertical
  i = 1;
  count = 0;
  if (row <= 2){
    while (i <= num - 1){
      if (board[row + i][col] == c) count++;
      i++;
    }
  }
  if (count >= num - 1) return true;
  //cout << "V" << count << endl;
  return false;
}

void Board::display(int w, int h){

  gfx_color(255, 255, 0);
  gfx_fill_rectangle(0, 0, w, h);
  int dw = w/7;
  int dh = h/6;

  gfx_flush();

  for (int i = 0; i < 6; i++){
    for (int j = 0; j < 7; j++){
      if (board[i][j] == 'b') gfx_color(0, 0, 0);
      else if (board[i][j] == 'r') gfx_color(255, 0, 0);
      else gfx_color(255, 255, 255);
      gfx_fill_circle(dw*(j+.5), dh*(i+.5), dw/2 - 5);
    }
  }
  gfx_flush();
}

void Board::clear(){
  for (int i = 0; i < 6; i ++){
    for (int j = 0; j < 7; j++){
      board[i][j] = 'o';
    }
  }
}
