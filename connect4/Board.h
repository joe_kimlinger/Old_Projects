// Joe Kimlinger
using namespace std;

class Board{
  public:
    Board();
    ~Board();
    int drop(int, char);	// 0 if cant be placed
    int cpuDrop(int);		// 2 if makes 4
    bool check(int, int, char, int);
    void display(int, int);
    bool full();
    void clear();
  private:
    char board[6][7];
};
