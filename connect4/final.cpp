// Joe Kimlinger
#include "gfxnew.h"
#include "Board.h"
#include <cmath>
#include <unistd.h>
#include <iostream>
using namespace std;

int width = 700;
int height = 6 * width / 7;
void startScreenDisplay();
void difficultyScreen();
void winScreen(int);

int main(){
  gfx_open(width, height, "Connect 4");

  Board board;
  char cmd;
  char z;
  int event;
  int turn;
  int place;
  int winner;
  int mode;
  int difficulty;
  char pcolor;

  // This loop runs infinitely with no break statements to exit it
  while (true){
    // Start menu loop
    while (true){
      startScreenDisplay();
      cmd = gfx_wait();
      // Check where click is (on which button)
      if (cmd == 1 && 
            gfx_xpos() >= width/9 && gfx_xpos() <= 4*width/9 && 
            gfx_ypos() >= 2*height/5 && gfx_ypos() <= 3*height/5){
        mode = 1;
        break;
      }
      else if (cmd == 1 && 
            gfx_xpos() >= 5*width/9 && gfx_xpos() <= 8*width/9 && 
            gfx_ypos() >= 2*height/5 && gfx_ypos() <= 3*height/5){
        mode = 2;
        break;
      }
  
    }

    // If in 1 player mode, set difficulty
    if (mode == 1)
      while (true){
        difficultyScreen();
        cmd = gfx_wait();
        if (cmd == '1'){
          difficulty = 1;
          break;
        }
        if (cmd == '2'){
          difficulty = 2;
          break;
        }
      }    
    board.display(width, height);
    turn = 0;
 
    // Game loop runs until someone wins
    while (true){
      // Set the color of the current piece every turn
      if (turn % 2 == 0) pcolor = 'r';
      else pcolor = 'b';
      // if 1 player mode and odd number turn, make ai move
      if (mode == 1 && turn %  2 == 1){
        place = board.cpuDrop(difficulty);	// ai move
        board.display(width, height);
        if (place == 2){
          winner = 3;	
          break;
        }
        turn++;
      }
      if (event = gfx_event_waiting()){
        // Wait for mouse click
        if (event == 3){
          cmd = gfx_wait();
          // if in 1 player and even turn number or if 2 player mode
          if ((cmd == 1) && (mode == 2 || pcolor == 'r')){
            // Place piece in column of click
            place = board.drop(gfx_xpos() * 7/width, pcolor);
          }
          board.display(width, height);
          // place = 2 when user gets 4 in a row
          if (place == 2){
            if (turn % 2 == 0) winner = 1;
            else winner = 2;
            break;
          }
          if (board.full()){
            winner = 4;
            break;
          }
          turn++;
        }
      else if (event == 1) z = gfx_wait();
      else if (event == 2) z = gfx_wait(); 
      else if (event == 4) z = gfx_wait();
      else if (event == 5) z == gfx_wait(); 
    }
  }
    
    // ending win screen
    while (true){
      winScreen(winner);
      usleep(5000000);
      board.clear();
      gfx_clear();
      break;
    }
  }
}

// Displays start screen with two buttons
void startScreenDisplay(){
  char greeting[] = "Weclome to Connect Four!";
  char gamemode[] = "Select your game Mode";
  char onep[] = "One Player";
  char twop[] = "Two Player";
  char font[] = "lucidasans-bold-24";
  char font2[] = "lucidasans-bold-14";
  int dx;
  int dy;

  gfx_changefont(font);
  gfx_color(255, 255, 0);
  gfx_fill_rectangle(0, 0, width, height);

  gfx_color(255, 0, 0);
  gfx_fill_rectangle(width/9, 2*height/5, width/3, height/5);
  gfx_fill_rectangle(5*width/9, 2*height/5, width/3, height/5);

  gfx_color(0, 0, 0);
  dx = gfx_textpixelwidth(greeting, font)/2;
  gfx_text(width/2 - dx, height/5, greeting);

  // dx and dy help set text in middle of rects
  dx = gfx_textpixelwidth(onep, font)/2;
  dy = gfx_textpixelheight(onep, font)/2;
  gfx_text(5*width/18 - dx, height/2, onep);
  dx = gfx_textpixelwidth(twop, font)/2;
  gfx_text(13*width/18 - dx, height/2, twop);

  gfx_changefont(font2);
  dx = gfx_textpixelwidth(gamemode, font2)/2;
  gfx_text(width/2 - dx, height/3, gamemode);
}

// Choose difficulty
void difficultyScreen(){
  char text[] = "Select Difficulty";
  char text2[] = "Press 1 for hard.  Press 2 for harder";
  char font[] = "lucidasans-bold-24";
  char font2[] = "lucidasans-bold-18";
  gfx_changefont(font);
  int dx;
  
  gfx_color(255, 255, 0);
  gfx_fill_rectangle(0, 0, width, height);

  gfx_color(0, 0, 0);
  dx = gfx_textpixelwidth(text, font)/2;
  gfx_text(width/2 - dx, height/3, text);

  gfx_changefont(font2);
  dx = gfx_textpixelwidth(text2, font2)/2;
  gfx_text(width/2 - dx, height/2, text2);
  gfx_flush();
}

void winScreen(int winner){
  char win1[] = "Player 1 wins!!!";
  char win2[] = "Player 2 wins!!!";
  char win3[] = "CPU wins!!!";
  char win4[] = "No one wins...";
  char font[] = "lucidasans-bold-24";
  gfx_changefont(font);
  int dx;
  int dy = gfx_textpixelheight(win1, font)/2;
  gfx_color(0, 0, 255);

  // Different text based on who wins displays over winning board
  if (winner == 1){
    dx = gfx_textpixelwidth(win1, font)/2;
    gfx_text(width/2 - dx, height/2 - dy, win1);
  }
  else if (winner == 2){
    dx = gfx_textpixelwidth(win2, font)/2;
    gfx_text(width/2 - dx, height/2 - dy, win2);
  }
  else if (winner == 3){
    dx = gfx_textpixelwidth(win3, font)/2;
    gfx_text(width/2 - dx, height/2 - dy, win3);
  }
  else {
    dx = gfx_textpixelwidth(win4, font)/2;
    gfx_text(width/2 - dx, height/2 - dy, win4);
  }
  gfx_flush();
}
