function [ship,newBoard] = randomShipPlacer(name, board,axes);
%   newBoard = shipPlacer();
%       randomShipPlacer places specified ship in random spot without
%       overlapping previously placed ships and returns the new board with
%       ships placed as 1s and a ship object with its position stored in ship 
%       under ship.position
%           Enter ship name as name and current board as board
%% Check Ship Type
newBoard = board;
name = lower(name);
placed = false;
length = shipLength(name);
if length == 0;
    placed = true
end
ship.name = name;

%% Placing
while ~placed
    direction = randi(2);           % Horizontal or Vertical
    if direction == 1;
    % Vertical
        row = randi(10 - length);
        col = randi(10);
        sum = 0;
        for ir = 0:length - 1
            sum = sum + board(row + ir,col);    % Check for previosly placed ship
        end
        if sum == 0;
            for i = 0:length - 1
                ship(i + 1).position = [row + i,col];
                newBoard(row + i,col) = 1;
            end
            placed = true;
        end
    else
    % Horizontal
        row = randi(10);
        col = randi(10 - length);
        sum = 0;
        for ic = 0:length - 1
            sum = sum + board(row,col + ic);    % Check for previosly placed ship
        end
        if sum == 0;
            for i = 0:length - 1
                ship(i + 1).position = [row,col + i];
                newBoard(row,col + i) = 1;
            end
            placed = true;
        end
    end
end

