function playerSetup(handles)

board = evalin('base','setupBoard');
placedShips = evalin('base','placedShips');
switch handles.shipPopup.Value
    case 1
        ship.name = 'carrier';
        length = 5;
        index = 5;
    case 2
        ship.name = 'battleship';
        length = 4;
        index = 4;
    case 3
        ship.name = 'destroyer';
        length = 4;
        index = 3;
    case 4
        ship.name = 'submarine';
        length = 3;
        index = 2;
    case 5
        ship.name = 'patrol';
        length = 2;
        index = 1;
end

column = lower(handles.setupColumnText.String);
column = double(column) - 96;
row = str2double(handles.setupRowText.String);
numPlacedShips = evalin('base','numPlacedShips');

if ~handles.verticalCheckbox.Value
    place = 0;
    if column + length - 1 <= 10 && numPlacedShips(index) == 0
        for icol = 0:length - 1
            place = place + board(row,column + icol);
        end
        if place == 0;
            for icol = 0:length - 1
                plot(handles.setupAxes,column - .5 + icol,10 - row + .5,'sk','MarkerSize',20);
                grid(handles.setupAxes,'on');
                axis(handles.setupAxes,[0,10,0,10]);
                set(handles.setupAxes,'XTickLabels','','YTickLabel','');
                hold(handles.setupAxes,'on');
                board(row,column + icol) = 1;
                ship(icol + 1).position = [row, column + icol];
            end
            numPlacedShips(index) = 1;
            placedShips{index} = ship;
        end
    end
else
    place = 0;
    if row + length -1 <= 10 && numPlacedShips(index) == 0
        for irow = 0:length - 1
            place = place + board(row + irow,column);
        end
        if place == 0;
            for irow = 0:length - 1
                plot(handles.setupAxes,column - .5,10 - row + .5 - irow,'sk','MarkerSize',20);
                grid(handles.setupAxes,'on');
                axis(handles.setupAxes,[0,10,0,10]);
                set(handles.setupAxes,'XTickLabels','','YTickLabel','');
                hold(handles.setupAxes,'on');
                board(row + irow,column) = 1;
                ship(irow + 1).position = [row + irow, column];
            end
            numPlacedShips(index) = 1;
            placedShips{index} = ship;
        end
    end
end
assignin('base','setupBoard',board);
if sum(numPlacedShips) == 5
    PlayerTool(handles);
    close('PlayerSetupTool');
end
assignin('base','numPlacedShips',numPlacedShips);
assignin('base','placedShips',placedShips);
    
end