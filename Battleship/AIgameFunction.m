function AIgameFunction(handles)
% Plays the game of battleship by calling each difficulty function once 
% for each turn.  Is synced with the AITool GUI to recieve inputs on the 
% number of games to be played and outputs the wins of each difficulty
% function.

%% Initialize Full Simulation Variables
% These variables deal with the simulatiton as a whole and must be
% initialized outside of the for loop
cpu1wins = 0;
cpu2wins = 0;
handles.cpu1WinsText.String = '0';
handles.cpu2WinsText.String = '0';
gamesPlayed = 0;

%% Main For Loop
% Runs desired number of games
for ir = 1:str2double(handles.runsText.String);
    
%% Set up ships
board1 = zeros(10);         % Initialize CPU1 board
fieldcreateF;               % Initialize hard difficulty probabilities
% Place all ships
[carrier1,board1] = randomShipPlacer('Carrier',board1,handles.cpu1Axes);         
[battleship1,board1] = randomShipPlacer('battleship',board1,handles.cpu1Axes);
[destroyer1,board1] = randomShipPlacer('Destroyer',board1,handles.cpu1Axes);
[submarine1,board1] = randomShipPlacer('Submarine',board1,handles.cpu1Axes);
[patrol1,board1] = randomShipPlacer('Patrol',board1,handles.cpu1Axes);
board1Ships = {carrier1, battleship1, destroyer1, submarine1, patrol1};

board2 = zeros(10);         % Initialize CPU2 board
% Place all ships
[carrier2,board2] = randomShipPlacer('Carrier',board2,handles.cpu2Axes);
[battleship2,board2] = randomShipPlacer('battleship',board2,handles.cpu2Axes);
[destroyer2,board2] = randomShipPlacer('Destroyer',board2,handles.cpu2Axes);
[submarine2,board2] = randomShipPlacer('Submarine',board2,handles.cpu2Axes);
[patrol2,board2] = randomShipPlacer('Patrol',board2,handles.cpu2Axes);
board2Ships = {carrier2, battleship2, destroyer2, submarine2, patrol2};

%% Initialize Game Specific Values
% These values must be intialized every time a new game is started
game = true;
sunkenShips1 = {'no','no','no','no','no'};
sunkenShips2 = {'no','no','no','no','no'};
ships1sunk = 0;
ships2sunk = 0;
moves1 = 0;
moves2 = 0; 

%% Place Ships as black Squares 
for irow = 1:10
    for icol = 1:10
        if board1(irow,icol) == 1
            axis(handles.cpu1Axes,[0,10,0,10]);
            plot(handles.cpu1Axes,icol - .5,10 - irow + .5,...
                'sk','MarkerSize',20);
            hold(handles.cpu1Axes,'on');
        end
        if board2(irow,icol) == 1
            axis(handles.cpu2Axes,[0,10,0,10]);
            plot(handles.cpu2Axes,icol - .5,10 - irow + .5,...
                'sk','MarkerSize',20);
            hold(handles.cpu2Axes,'on');
        end
    end
end

%% Game Loop
% Each iteration represents one "turn"
while game
    % Guess spot on cpu1 board using difficulty function determined by
    % StartScreenTool
    % this is equvilant to the guess of cpu2
    if evalin('base','difficulty2') == 1;
        board1 = easyDifficulty(board1,handles.cpu1Axes);
        moves1 = moves1 + 1;
        handles.moves1Text.String = num2str(moves1);
    elseif evalin('base','difficulty2') == 2;
        board1 = mediumDifficulty(board1,handles.cpu1Axes);
        moves1 = moves1 + 1;
        handles.moves1Text.String = num2str(moves1);
    else
        board1 = hardDifficulty(board1,handles.cpu1Axes);
        moves1 = moves1 + 1;
        handles.moves1Text.String = num2str(moves1);
    end
    
    % Checks each ship to see if it is sunk on board 1
    for iship = 1:5
        ship1 = board1Ships{iship};
        length1 = shipLength(ship1(1).name);

        % Sum values at coordinates of ship on cpu1 board
        sum1 = 0;
        for it = 1:length1;
            row = ship1(it).position(1);
            col = ship1(it).position(2);
             if board1(row,col) ~= 1;
                 sum1 = sum1 + 1;
             end
             % If it is sunk, change its value in the sunkenShips array to
             % sunk and change its coordinates to 6s
             if sum1 == length1 && ~strcmp(sunkenShips1{iship},'sunk')
                 sunkenShips1{iship} = 'sunk';
                 ships1sunk = ships1sunk + 1;
                 for ispot = 1:length1
                    board1(ship1(ispot).position(1),ship1(ispot).position(2)) = 6;
                end
             end                 
        end
        
        % if all ships are sunk, the game ends
        if ships1sunk == 5
            win = 2;
            game = false;
            break
        end
    end
    
    % Guess spot on cpu2 board using easy difficulty function
    % this is equvilant to the guess of cpu1
    if game == false
        break
    end
    
    if evalin('base','difficulty1') == 2;
        board2 = easyDifficulty(board2,handles.cpu2Axes);
        moves2 = moves2 + 1;
        handles.moves2Text.String = num2str(moves2);
    elseif evalin('base','difficulty1') == 3;
        board2 = mediumDifficulty(board2,handles.cpu2Axes);
        moves2 = moves2 + 1;
        handles.moves2Text.String = num2str(moves2);
    elseif evalin('base','difficulty1') == 4;
        board2 = hardDifficulty(board2,handles.cpu2Axes);
        moves2 = moves2 + 1;
        handles.moves2Text.String = num2str(moves2);
    end
    
    % Iterate through each ship to check if its sunk on board 2
    for iship = 1:5;
        ship2 = board2Ships{iship};
        length2 = shipLength(ship2(1).name);
        
        % Sum values at coordinates of ship on cpu2 board
        sum2 = 0;
        for it = 1:length2;
            row = ship2(it).position(1);
            col = ship2(it).position(2);
             if board2(row,col) ~= 1;
                 sum2 = sum2 + 1;
             end
             % If it is sunk, change its value in the sunkenShips array to
             % sunk and change its coordinates to 6s
             if sum2 == length2 && ~strcmp(sunkenShips2{iship},'sunk')
                 sunkenShips2{iship} = 'sunk';
                 ships2sunk = ships2sunk + 1;
                 for ispot = 1:length2
                    board2(ship2(ispot).position(1),ship2(ispot).position(2)) = 6;
                 end
             end                 
        end
        
        % If all the ships are sunk, game ends
        if ships2sunk == 5
            game = false;
            win = 1;
            break
        end
    end
end

% Add the win to the tally
if win == 1
    cpu1wins = cpu1wins + 1;
    handles.cpu1WinsText.String = num2str(cpu1wins);
elseif win == 2
    cpu2wins = cpu2wins + 1;
    handles.cpu2WinsText.String = num2str(cpu2wins);
end

% Clears board and adds to the game tally
% Game ends
hold(handles.cpu1Axes,'off');
hold(handles.cpu2Axes,'off');
clear handles.cpu1Axes;
clear handles.cpu2Axes;
gamesPlayed = gamesPlayed + 1;
handles.gamesPlayedText.String = num2str(gamesPlayed);
end
end

    

