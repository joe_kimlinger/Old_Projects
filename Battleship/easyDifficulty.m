function newboard = easyDifficulty(board,axes)
% Takes unputs board and axes and returns an updated board with a single 
% shot made along with the shot location on the set GUI axes

%% Initialize
newboard = board;
done = false;

while ~done
    %Makes a guess
    row = randi(10);
    col = randi(10);
    guess = newboard(row,col);
    % Sets position = 3 if a miss and = 2 if a hit
    if guess == 0;
        newboard(row,col) = 3;
        plot(axes,col - .5,10 - row + .5,'ob','MarkerSize',20);
        axis(axes,[0,10,0,10]);
        set(axes,'XTickLabels','','YTickLabel','');
        grid(axes,'on');
        hold(axes,'on');
        drawnow;
        done = true;
    elseif guess == 1;
        newboard(row,col) = 2;
        plot(axes,col - .5,10 - row + .5,'xr','MarkerSize',20);
        axis(axes,[0,10,0,10]);
        set(axes,'XTickLabels','','YTickLabel','');
        grid(axes,'on');
        hold(axes,'on');
        drawnow;
        done = true;
    end
end
