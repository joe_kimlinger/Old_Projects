function varargout = PlayerSetupTool(varargin)
% PLAYERSETUPTOOL MATLAB code for PlayerSetupTool.fig
%      PLAYERSETUPTOOL, by itself, creates a new PLAYERSETUPTOOL or raises the existing
%      singleton*.
%
%      H = PLAYERSETUPTOOL returns the handle to a new PLAYERSETUPTOOL or the handle to
%      the existing singleton*.
%
%      PLAYERSETUPTOOL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PLAYERSETUPTOOL.M with the given input arguments.
%
%      PLAYERSETUPTOOL('Property','Value',...) creates a new PLAYERSETUPTOOL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before PlayerSetupTool_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to PlayerSetupTool_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help PlayerSetupTool

% Last Modified by GUIDE v2.5 19-Apr-2016 13:07:35

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @PlayerSetupTool_OpeningFcn, ...
                   'gui_OutputFcn',  @PlayerSetupTool_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before PlayerSetupTool is made visible.
function PlayerSetupTool_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to PlayerSetupTool (see VARARGIN)

% Choose default command line output for PlayerSetupTool
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes PlayerSetupTool wait for user response (see UIRESUME)
% uiwait(handles.figure1);
clear('handles.setupAxes');
assignin('base','numPlacedShips',[0,0,0,0,0]);
assignin('base','placedShips',{0,0,0,0,0});
assignin('base','setupBoard',zeros(10));


% --- Outputs from this function are returned to the command line.
function varargout = PlayerSetupTool_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in verticalCheckbox.
function verticalCheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to verticalCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of verticalCheckbox


% --- Executes on selection change in shipPopup.
function shipPopup_Callback(hObject, eventdata, handles)
% hObject    handle to shipPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns shipPopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from shipPopup


% --- Executes during object creation, after setting all properties.
function shipPopup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to shipPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function setupRowText_Callback(hObject, eventdata, handles)
% hObject    handle to setupRowText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of setupRowText as text
%        str2double(get(hObject,'String')) returns contents of setupRowText as a double


% --- Executes during object creation, after setting all properties.
function setupRowText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to setupRowText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function setupColumnText_Callback(hObject, eventdata, handles)
% hObject    handle to setupColumnText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of setupColumnText as text
%        str2double(get(hObject,'String')) returns contents of setupColumnText as a double


% --- Executes during object creation, after setting all properties.
function setupColumnText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to setupColumnText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in placeButton.
function placeButton_Callback(hObject, eventdata, handles)
% hObject    handle to placeButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if str2double(handles.setupRowText.String) > 0 && str2double(handles.setupRowText.String) <= 10 && double(lower(handles.setupColumnText.String)) >= 97 && double(lower(handles.setupColumnText.String)) <= 106
    playerSetup(handles);
end


% --- Executes on button press in backButton.
function backButton_Callback(hObject, eventdata, handles)
% hObject    handle to backButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
startScreenTool(handles);
close(PlayerSetupTool);
