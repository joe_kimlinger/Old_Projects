function varargout = AITool(varargin)
% AITOOL MATLAB code for AITool.fig
%      AITOOL, by itself, creates a new AITOOL or raises the existing
%      singleton*.
%
%      H = AITOOL returns the handle to a new AITOOL or the handle to
%      the existing singleton*.
%
%      AITOOL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in AITOOL.M with the given input arguments.
%
%      AITOOL('Property','Value',...) creates a new AITOOL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before AITool_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to AITool_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help AITool

% Last Modified by GUIDE v2.5 20-Apr-2016 17:59:56

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @AITool_OpeningFcn, ...
                   'gui_OutputFcn',  @AITool_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before AITool is made visible.
function AITool_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to AITool (see VARARGIN)

% Choose default command line output for AITool
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes AITool wait for user response (see UIRESUME)
% uiwait(handles.figure1);
set(handles.cpu1Axes,'XTickLabel','','YTickLabel','');
set(handles.cpu2Axes,'XTickLabel','','YTickLabel','');
clear('handles.cpu1Axes');
clear('handles.cpu2Axes');
if evalin('base','difficulty2') == 1;
    handles.p2ModeText.String = 'Easy';
elseif evalin('base','difficulty2') == 2;
    handles.p2ModeText.String = 'Medium';
else
    handles.p2ModeText.String = 'Hard';
end
if evalin('base','difficulty1') == 2;
    handles.p1ModeText.String = 'Easy';
elseif evalin('base','difficulty1') == 3;
    handles.p1ModeText.String = 'Medium';
elseif evalin('base','difficulty1') == 4;
    handles.p1ModeText.String = 'Hard';
end



% --- Outputs from this function are returned to the command line.
function varargout = AITool_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function moves1Text_Callback(hObject, eventdata, handles)
% hObject    handle to moves1Text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of moves1Text as text
%        str2double(get(hObject,'String')) returns contents of moves1Text as a double


% --- Executes during object creation, after setting all properties.
function moves1Text_CreateFcn(hObject, eventdata, handles)
% hObject    handle to moves1Text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function moves2Text_Callback(hObject, eventdata, handles)
% hObject    handle to moves2Text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of moves2Text as text
%        str2double(get(hObject,'String')) returns contents of moves2Text as a double


% --- Executes during object creation, after setting all properties.
function moves2Text_CreateFcn(hObject, eventdata, handles)
% hObject    handle to moves2Text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in runButton.
function runButton_Callback(hObject, eventdata, handles)
% hObject    handle to runButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
AIgameFunction(handles);



function runsText_Callback(hObject, eventdata, handles)
% hObject    handle to runsText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of runsText as text
%        str2double(get(hObject,'String')) returns contents of runsText as a double


% --- Executes during object creation, after setting all properties.
function runsText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to runsText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function cpu1WinsText_Callback(hObject, eventdata, handles)
% hObject    handle to cpu1WinsText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of cpu1WinsText as text
%        str2double(get(hObject,'String')) returns contents of cpu1WinsText as a double


% --- Executes during object creation, after setting all properties.
function cpu1WinsText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cpu1WinsText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function cpu2WinsText_Callback(hObject, eventdata, handles)
% hObject    handle to cpu2WinsText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of cpu2WinsText as text
%        str2double(get(hObject,'String')) returns contents of cpu2WinsText as a double


% --- Executes during object creation, after setting all properties.
function cpu2WinsText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cpu2WinsText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in backButton.
function backButton_Callback(hObject, eventdata, handles)
% hObject    handle to backButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
startScreenTool(handles);
close(AITool);



function p1ModeText_Callback(hObject, eventdata, handles)
% hObject    handle to p1ModeText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of p1ModeText as text
%        str2double(get(hObject,'String')) returns contents of p1ModeText as a double


% --- Executes during object creation, after setting all properties.
function p1ModeText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to p1ModeText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function p2ModeText_Callback(hObject, eventdata, handles)
% hObject    handle to p2ModeText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of p2ModeText as text
%        str2double(get(hObject,'String')) returns contents of p2ModeText as a double


% --- Executes during object creation, after setting all properties.
function p2ModeText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to p2ModeText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function gamesPlayedText_Callback(hObject, eventdata, handles)
% hObject    handle to gamesPlayedText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of gamesPlayedText as text
%        str2double(get(hObject,'String')) returns contents of gamesPlayedText as a double


% --- Executes during object creation, after setting all properties.
function gamesPlayedText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gamesPlayedText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
