function length = shipLength(name)
% Takes a string as an input and returns the length of that ship as a
% double
% If the name is not recognized, length = 0
name = lower(name);
switch name
    case 'carrier'
        length = 5;
    case 'battleship'
        length = 4;
    case 'destroyer'
        length = 4;
    case 'submarine'
        length = 3;
    case 'patrol'
        length = 2;
    otherwise
        length = 0;
end