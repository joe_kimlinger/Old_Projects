function varargout = startScreenTool(varargin)
% STARTSCREENTOOL MATLAB code for startScreenTool.fig
%      STARTSCREENTOOL, by itself, creates a new STARTSCREENTOOL or raises the existing
%      singleton*.
%
%      H = STARTSCREENTOOL returns the handle to a new STARTSCREENTOOL or the handle to
%      the existing singleton*.
%
%      STARTSCREENTOOL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in STARTSCREENTOOL.M with the given input arguments.
%
%      STARTSCREENTOOL('Property','Value',...) creates a new STARTSCREENTOOL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before startScreenTool_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to startScreenTool_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help startScreenTool

% Last Modified by GUIDE v2.5 05-Apr-2016 19:22:51

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @startScreenTool_OpeningFcn, ...
                   'gui_OutputFcn',  @startScreenTool_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before startScreenTool is made visible.
function startScreenTool_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to startScreenTool (see VARARGIN)

% Choose default command line output for startScreenTool
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes startScreenTool wait for user response (see UIRESUME)
% uiwait(handles.figure1);



% --- Outputs from this function are returned to the command line.
function varargout = startScreenTool_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in difficultyPopup1.
function difficultyPopup1_Callback(hObject, eventdata, handles)
% hObject    handle to difficultyPopup1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns difficultyPopup1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from difficultyPopup1


% --- Executes during object creation, after setting all properties.
function difficultyPopup1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to difficultyPopup1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in difficultyPopup2.
function difficultyPopup2_Callback(hObject, eventdata, handles)
% hObject    handle to difficultyPopup2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns difficultyPopup2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from difficultyPopup2


% --- Executes during object creation, after setting all properties.
function difficultyPopup2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to difficultyPopup2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in startButton.
function startButton_Callback(hObject, eventdata, handles)
% hObject    handle to startButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
assignin('base','difficulty1',handles.difficultyPopup1.Value);
assignin('base','difficulty2',handles.difficultyPopup2.Value);

% If the first popup is set to human, run player game function
if handles.difficultyPopup1.Value == 1;
    PlayerSetupTool(handles);
    close(startScreenTool);
else
    AITool(handles);
    close(startScreenTool);
end
    
