function varargout = PlayerTool(varargin)
% PLAYERTOOL MATLAB code for PlayerTool.fig
%      PLAYERTOOL, by itself, creates a new PLAYERTOOL or raises the existing
%      singleton*.
%
%      H = PLAYERTOOL returns the handle to a new PLAYERTOOL or the handle to
%      the existing singleton*.
%
%      PLAYERTOOL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PLAYERTOOL.M with the given input arguments.
%
%      PLAYERTOOL('Property','Value',...) creates a new PLAYERTOOL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before PlayerTool_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to PlayerTool_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help PlayerTool

% Last Modified by GUIDE v2.5 19-Apr-2016 16:18:17

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @PlayerTool_OpeningFcn, ...
                   'gui_OutputFcn',  @PlayerTool_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before PlayerTool is made visible.
function PlayerTool_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to PlayerTool (see VARARGIN)

% Choose default command line output for PlayerTool
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes PlayerTool wait for user response (see UIRESUME)
% uiwait(handles.figure1);
clear('handles.playerAxes')
clear('handles.cpuAxes')
%% Initialize Values
assignin('base','ships1sunk',0);
assignin('base','ships2sunk',0);
assignin('base','game',true);
fieldcreateF; 

%% Initialize Graphs
% Graph 1
axis(handles.playerAxes,[0,10,0,10]);
set(handles.playerAxes,'XTickLabels','','YTickLabel','');
grid(handles.playerAxes,'on');
hold(handles.playerAxes,'on');
% Graph 2
axis(handles.cpuAxes,[0,10,0,10]);
set(handles.cpuAxes,'XTickLabels','','YTickLabel','');
grid(handles.cpuAxes,'on');
hold(handles.cpuAxes,'on');

%% Initialize Boards
board1 = evalin('base','setupBoard');
assignin('base','board1',board1);
assignin('base','board2',zeros(10));

%% Place cpu Ships
[carrier,board2] = randomShipPlacer('Carrier',evalin('base','board2'));
[battleship,board2] = randomShipPlacer('battleship',board2);
[destroyer,board2] = randomShipPlacer('Destroyer',board2);
[submarine,board2] = randomShipPlacer('Submarine',board2);
[patrol,board2] = randomShipPlacer('Patrol',board2);
board2Ships = {patrol,submarine,destroyer,battleship,carrier};
assignin('base','board2Ships',board2Ships);
assignin('base','board2',board2);
assignin('base','sunkenShips1',{'no','no','no','no','no'});
assignin('base','sunkenShips2',{'no','no','no','no','no'});


% --- Outputs from this function are returned to the command line.
function varargout = PlayerTool_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in verticalCheckbox.
function verticalCheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to verticalCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of verticalCheckbox


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rowText_Callback(hObject, eventdata, handles)
% hObject    handle to rowText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rowText as text
%        str2double(get(hObject,'String')) returns contents of rowText as a double


% --- Executes during object creation, after setting all properties.
function rowText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rowText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function colText_Callback(hObject, eventdata, handles)
% hObject    handle to colText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of colText as text
%        str2double(get(hObject,'String')) returns contents of colText as a double


% --- Executes during object creation, after setting all properties.
function colText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to colText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in shootButton.
function shootButton_Callback(hObject, eventdata, handles)
% hObject    handle to shootButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if str2double(handles.rowText.String) > 0 && str2double(handles.rowText.String) <= 10 && double(lower(handles.colText.String)) >= 97 && double(lower(handles.colText.String)) <= 106 && evalin('base','game')
    playerGameFunction(handles);
end



function playerMovesText_Callback(hObject, eventdata, handles)
% hObject    handle to playerMovesText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of playerMovesText as text
%        str2double(get(hObject,'String')) returns contents of playerMovesText as a double


% --- Executes during object creation, after setting all properties.
function playerMovesText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to playerMovesText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function cpuMovesText_Callback(hObject, eventdata, handles)
% hObject    handle to cpuMovesText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of cpuMovesText as text
%        str2double(get(hObject,'String')) returns contents of cpuMovesText as a double


% --- Executes during object creation, after setting all properties.
function cpuMovesText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cpuMovesText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function messageText_Callback(hObject, eventdata, handles)
% hObject    handle to messageText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of messageText as text
%        str2double(get(hObject,'String')) returns contents of messageText as a double


% --- Executes during object creation, after setting all properties.
function messageText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to messageText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in backButton.
function backButton_Callback(hObject, eventdata, handles)
% hObject    handle to backButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
PlayerSetupTool(handles);
close(PlayerTool);
