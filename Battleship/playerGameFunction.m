function playerGameFucntion(handles)
% Runs thorugh 1 turns for each call

%% Retrieve constant variabes stored in the base workspace
board1 = evalin('base','board1');
board2 = evalin('base','board2');
sunkenShips1 = evalin('base','sunkenShips1');
sunkenShips2 = evalin('base','sunkenShips2');
ships1sunk = evalin('base','ships1sunk');
ships2sunk = evalin('base','ships2sunk');
board1Ships = evalin('base','placedShips');
board2Ships = evalin('base','board2Ships');
lengths = [2,3,4,4,5];

%% Plot players ships as black squares
for irow = 1:10
    for icol = 1:10
        if board1(irow,icol) == 1;
            plot(handles.playerAxes,icol - .5,10 - irow + .5,'sk','MarkerSize',20);
            axis(handles.playerAxes,[0,10,0,10]);
            set(handles.playerAxes,'XTickLabels','','YTickLabel','');
            grid(handles.playerAxes,'on');
            hold(handles.playerAxes,'on');
            drawnow;
        end
        
    end
end

%% Get user inputs 
row = str2double(handles.rowText.String);
col = lower(handles.colText.String);
col = double(col) - 96;
shot = board2(row,col);


%% Player 
% Updadte cpu board for user shot
if shot == 0;
    handles.messageText.String = 'Miss';
    board2(row,col) = 3;
    plot(handles.cpuAxes,col - .5,10 - row + .5,'ob','MarkerSize',20);
    axis(handles.cpuAxes,[0,10,0,10]);
    set(handles.cpuAxes,'XTickLabels','','YTickLabel','');
    grid(handles.cpuAxes,'on');
    hold(handles.cpuAxes,'on');
    drawnow;
elseif shot == 1;
    handles.messageText.String = 'Hit!';
    board2(row,col) = 2;
    plot(handles.cpuAxes,col - .5,10 - row + .5,'xr','MarkerSize',20)
    plot(handles.cpuAxes,col - .5,10 - row + .5,'sk','MarkerSize',20);
    axis(handles.cpuAxes,[0,10,0,10]);
    set(handles.cpuAxes,'XTickLabels','','YTickLabel','');
    grid(handles.cpuAxes,'on');
    hold(handles.cpuAxes,'on');
    drawnow;
end

% Add a move to player moves
handles.playerMovesText.String = num2str(str2double(handles.playerMovesText.String) + 1);

% Check for sunken cpu ships
for iship = 1:5
    ship2 = board2Ships{iship};
    length2 = lengths(iship);
    
    % Sum values at coordinates of ship on cpu1 board
    sum1 = 0;
    for it = 1:length2;
        row = ship2(it).position(1);
        col = ship2(it).position(2);
        if board2(row,col) ~= 1;
            sum1 = sum1 + 1;
        end
        % If ship is sunk, output message in GUI text and change
        % coordinates of ship to 6s
        if sum1 == length2 && ~strcmp(sunkenShips2{iship},'sunk')
            sunkenShips2{iship} = 'sunk';
            handles.messageText.String = ['You sunk the ',ship2.name,' boat!'];
            ships2sunk = ships2sunk + 1;
            for ispot = 1:length2
                board2(ship2(ispot).position(1),ship2(ispot).position(2)) = 6;
            end
        end
    end
    
    % If all ships are sunk, end the game
    if ships2sunk == 5
        assignin('base','game',false);
        handles.messageText.String = 'You Win!!!!';
        break
    end
end


%% CPU
% Check for difficulty and take a turn
if evalin('base','difficulty2') == 1;
    board1 = easyDifficulty(board1,handles.playerAxes);
elseif evalin('base','difficulty2') == 2;
    board1 = mediumDifficulty(board1,handles.playerAxes);
elseif evalin('base','difficulty2') == 3;
    board1 = hardDifficulty(board1,handles.playerAxes);
end

% Add a move to cpu moves
handles.cpuMovesText.String = num2str(str2double(handles.cpuMovesText.String) + 1);

% Check for sunken player ships
for iship = 1:5
    ship1 = board1Ships{iship};
    length1 = lengths(iship);
    
    % Sum values at coordinates of ship on player board
    sum1 = 0;
    for it = 1:length1;
        row = ship1(it).position(1);
        col = ship1(it).position(2);
        if board1(row,col) ~= 1;
            sum1 = sum1 + 1;
        end
        % If ship is sunk, output message in GUI text and change
        % coordinates of ship to 6s
        if sum1 == length1 && ~strcmp(sunkenShips1{iship},'sunk')
            sunkenShips1{iship} = 'sunk';
            ships1sunk = ships1sunk + 1;
            for ispot = 1:length1
                board1(ship1(ispot).position(1),ship1(ispot).position(2)) = 6;
            end
        end
    end
    
    % If all ships are sunk, end the game
    if ships1sunk == 5
        assignin('base','game',false);
        handles.messageText.String = 'You Lose';
        break
    end
end

% Reassign all variables used in the base workspace so they can be used
% again the next turn
assignin('base','board2',board2);
assignin('base','board1',board1);
assignin('base','ships2sunk',ships2sunk);
assignin('base','ships1sunk',ships1sunk);
assignin('base','sunkenShips1',sunkenShips1);
assignin('base','sunkenShips2',sunkenShips2);
end