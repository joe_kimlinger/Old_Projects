function field = fieldcreateF
% initial probablity is constant for every game
xmin = 0;
xmax = 10;
Nx = 10;
x = linspace(xmin, xmax, Nx);
% probablity distribution of ships on the board based off of a gaussian
% equation
y = .475*exp(-(x-5).^2/9);


% creates an initial blank distribution set
row = 10;
col=10;
field = zeros(row,col);
%% fills the array field with a
for ir=1:10
    for ic=1:10
        field(ir,ic) = field(ir,ic)+y(ic)+y(ir);
    end
end
assignin('base','field',field);