// Joe Kimlinger
#include <iostream>
#include <fstream>
#include "Board.h"
#include <string>
#include <random>
#include <vector>
#include <iomanip>
#include <ctime>
#include <fstream>
#include <algorithm>
using namespace std;

// Stores coordinates, orientation, adn anagram of each placed word
struct Coords {
  int row;
  int col;
  int dir;
  string anagram;
};

typedef vector<string>::iterator vecmover;
string makeAnagram(string&);
string lowered(string);
vector<string> inputWords(istream&);
void placefirstword(vector<string>&, vector<Coords>&, Board&);
vector<string> placewordlist(vector<string>&, vector<Coords>&, Board&);
void printHints(vector<Coords>&, ostream&);

// Two fucntions assist the sort function in defining sorting parameters
bool sortCred(string a, string b) {return a.length() > b.length();}
bool sortCoordinates(Coords a, Coords b) {return (a.col == b.col) ? a.row < b.row : a.col < b.col;}

int main(int argc, char *argv[]){
  Board board;
  Coords c;
  vector<Coords> coordinates;
  vector<string> words;
  vector<string> unplaced;
    
  // Greeting
  cout << "\nCrossword Anagram Puzzle Generator\n";
  cout << "----------------------------------\n\n";
 
  if (argc == 1 ){
    // Input words and feed into vector
    cout << "Enter up to 20 words of 15 characters or less: " << endl;
    words = inputWords(cin);
    cout << endl;
  }
  if (argc == 2 || argc == 3){
    ifstream ifile(argv[1]);
    if (!ifile){
      cout << "Error opening file" << endl;
      return 0;
    }
    words = inputWords(ifile);
  }
  if (argc == 3){
    ofstream ofile(argv[2]);
    sort (words.begin(), words.end(), sortCred);
    placefirstword(words, coordinates, board);
    unplaced = placewordlist(words, coordinates, board);
    for (string s : unplaced) ofile << s << " coud not be placed." << endl;

    ofile << endl << "Solution:\n\n";
    board.printSol(ofile);

    ofile << endl << "Puzzle\n\n";
    board.print(ofile); 

    printHints(coordinates, ofile); 

    return 0;
  }
  if (argc > 3) { 
    cout << "Too many command line arguments.";
    return 0;
  }

  // Sort words from longest to shortest
  sort (words.begin(), words.end(), sortCred);

  // Place words on board
  placefirstword(words, coordinates, board);
  unplaced = placewordlist(words, coordinates, board);
  for (string s : unplaced) cout << s << "coud not be placed." << endl;

  cout << endl << "Solution:\n\n";
  board.printSol(cout);

  cout << endl << "Puzzle\n\n";
  board.print(cout); 

  printHints(coordinates, cout); 
}


// returns a lowercase string without cahnging original
string lowered(string orig){
  string newstr = orig;
  int i = 0;
  for (int i = 0; i < newstr.length(); i ++){
    newstr[i] = tolower(newstr[i]);
  }
  return newstr;
}

// Returns a random anagram from input
string makeAnagram(string& word){
  string anagram = word;
  time_t t;
  unsigned seed = time(&t);		// Seeds random number generator
  shuffle (anagram.begin(), anagram.end(),default_random_engine(seed));
  return anagram;
}

vector<string> inputWords(istream& stream){
  bool add;
  int j = 0;
  string word;
  vector<string> words;

  stream >> word;
  // Runs until user enters . instead of a word
  while (word != "." && j < 20){
    add = true;
    // Checks each input to make sure it is valid and converts to uppercase
    for (char& it : word){
      it = toupper(it);
      if (it < 65 || it > 90 || word.length() > 15){
        cout << "That's not a valid word, try again." << endl;
        add = false;
        break;
      }
    }
    // If valid word, add to vector
    if (add){
      words.push_back(word);
      j++;
    }
    stream >> word;
  }
  return words;
}

// Places the first word in the middle of the board
void placefirstword(vector<string>& words, vector<Coords>& coordinates, Board& board){
  // Adds info from fisrt word to Coords struct
  Coords c;
  board.firstWord(words.front());	// Place word
  c.row = board.getRow();		// firstWord() defines row, col, dir
  c.col = board.getCol();
  c.dir = board.getDir();
  c.anagram = makeAnagram(words.front());
  coordinates.push_back(c);
}

// Places each word from vector until one cant be placed
vector<string> placewordlist(vector<string>& words, vector<Coords>& coordinates, Board& board){
  Coords c;
  vector<string> unplaced;
  for (vecmover it = words.begin()+1; it != words.end(); it++){
    // If word was placed, placeWord() will return true
    if(board.placeWord(*it)) {
      c.row = board.getRow();
      c.col = board.getCol();
      c.dir = board.getDir();
      c.anagram = makeAnagram(*it);
      coordinates.push_back(c);
    }
    else{
      unplaced.push_back(*it);
    }
  }
  return unplaced;
}

// Print row, col, direction, and anagram that serve as hints
void printHints(vector<Coords>& coordinates, ostream& stream){
  // Sort coordinates vector based forst on column then row
  sort(coordinates.begin(), coordinates.end(), sortCoordinates); 
  string d;
  stream << endl << "Clues:\n\n";
  for (Coords co : coordinates){
    stream << setw(2) << co.col+1 << "," << setw(2) << co.row+1 << " ";
    // Direction is determined by Board class, 1 for down and 2 for across
    (co.dir == 1) ? d = "Down" : d = "Across";
    stream << fixed << setw(7) << d << " " << co.anagram << endl;
  }
}
