// Joe Kimlinger
#include "Board.h"
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
using namespace std;

// Shortcuts for iterators
typedef string::iterator strmover;
typedef vector<string>::iterator vecmover;

Board::Board(){
  for (int i = 0; i < 15; i++){
    for (int j = 0; j < 15; j++){
      board[i][j] = '#';
    }
  }
}

Board::~Board(){};

char Board::getVal(int row, int col){
  return board[row][col];
}

void Board::setVal(int row, int col, char c){
  board[row][col] = c;
}

int Board::getRow(){
  return crow;
}

int Board::getCol(){
  return ccol;
}

int Board::getDir(){
  return cdir;
}

// Place word across in middle of board
void Board::firstWord(string word){
  first = word;
  int middle = word.length()/2;
  int i = 7 - middle;		// Match middle of board to middle of word
  crow = 6;
  ccol = i;
  cdir = 2;
  // Place word in row 6 in middle of board
  for (strmover it = word.begin(); it != word.end(); i++, it++){
    setVal(6, i, *it);
  }
}

// Place single word
bool Board::placeWord(string word){
  int dir;		// 1 = vertical  2 = horizontal
  // For every char in word, check whole board for a place to put it
  for (strmover it = word.begin(); it != word.end(); it++){
    for (int i = 0; i < 15; i++){
      for (int j = 0; j < 15; j++){
        // If board char is equal to current char in word, check path of word
        if (getVal(i, j) == *it){
          // If path is clear to place, set direction and place word
          if (dir = canBePlaced(i, j, it, word)){
            place(i, j, it, word, dir);
            return true;	// Return true to indicate word can be placed
          }
        }
      }
    }
  }
  return false;
}

// Check path of word based on curretn board position and position in word
int Board::canBePlaced(int row,int col,strmover it, string& word){
  // Check vertical placement
  strmover rt = it;	// temporary iterator ir to preserve value of it
  int trow = row;	// temporary row and col to preserve original values
  int tcol = col;
  int good = 0;

  // Check all 4 directions 
  while(getVal(trow+1, tcol) == '#' && getVal(trow+1, tcol-1) == '#' && getVal(trow+1, tcol+1) == '#'){
    // If ir gets to end of word without obstruction, there is room to place it
    if (rt == word.end() - 1){
      good++;
      break;
    }
    rt++;
    trow++;
  }
  // Reset row and rt to original
  trow = row;
  rt = it;
  while(getVal(trow-1, tcol) == '#' && getVal(trow-1, tcol-1) == '#' && getVal(trow-1, tcol+1) == '#'){
    // If ir gets to beginning of word without obstruction, there is room to plac
    if (rt == word.begin()){
      good++;
      break;
    }
    trow--;
    rt--;
  }
  // If word can be placed in both directions, return a 1, which means it has
  // room to be placed vertically
  if (good == 2) return 1;

  // Check horizontal
  // Follows saem logic as vertical
  rt = it;
  trow = row;
  good = 0;
  while(getVal(trow, tcol+1) == '#' && getVal(trow+1, tcol+1) == '#' && getVal(trow-1, tcol+1) == '#'){
    if (rt == word.end() - 1){
      good++;
      break;
    }
    rt++;
    tcol++;
  }
  tcol = col;
  rt = it;
  while(getVal(trow, tcol-1) == '#' && getVal(trow+1, tcol-1) == '#' && getVal(trow-1, tcol-1) == '#'){
    if (rt == word.begin()){
      good++;
      break;
    }
    tcol--;
    rt--;
  }
  if (good == 2) return 2;
  else return 0;
}

// Place word on board
void Board::place(int row, int col, strmover it, string& word, int dir){
  // Determine iterator position on word
  int dist = it - word.begin();
  it = word.begin();
  int dr, dc;
  // Vertical
  if (dir == 1){
    dc = 0;		// Increment row and not col
    dr = 1;
    row -= dist;	// Set to poistion dist above intersection of other word
  }
  else {
    dc = 1;
    dr = 0;
    col -= dist;	// Set to position dist right of intersection
  }
  // crow, ccol, and cdir 
  crow = row;		// Stow current row column and direction in class
  ccol = col;		// varibales for later use
  cdir = dir;
  for (int i = 0; i < word.length(); row+=dr, col+=dc, it++, i++){
    setVal(row, col, *it);
  }
}

// Place board with blanks where words are
void Board::print(ostream& stream){
  for (int i = 0; i < 15; i++){
    for (int j = 0; j < 15; j++){
      // If anything but # print blank space
      (getVal(i, j) == '#') ? stream << '#' : stream << ' ';
    }
    stream << endl;
  }
}

// Print board with words and .s
void Board::printSol(ostream& stream){
  for (int i = 0; i < 15; i++){
    for (int j = 0; j < 15; j++){
      // If a # print a . else print current char
      (getVal(i, j) == '#') ? stream << '.' : stream << getVal(i, j);
    }
    stream << endl;
  }
}
