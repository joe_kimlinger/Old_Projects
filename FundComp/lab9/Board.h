#include <iostream>
#include <string>
#include <vector>
#include <fstream>
using namespace std;

class Board{
  public:
    Board();
    ~Board();
    char getVal(int, int);
    void setVal(int, int, char);
    int getRow();
    int getCol();
    int getDir();
    void firstWord(string);
    bool placeWord(string);
    int canBePlaced(int, int, string::iterator, string&);
    void place(int, int, string::iterator, string&, int);
    void print(ostream&);
    void printSol(ostream&);
 private:
    char board[20][20];
    int crow;
    int ccol;
    int cdir;
    string first;
};
