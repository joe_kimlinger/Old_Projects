// Joe Kimlinger
#include <iostream>
#include <iomanip>
#include <cmath>	// Need a square root function
using namespace std;

int main(){

  int sum = 0;
  int nums [50];	// Max array size is 50
  int i = 0;	
  int in;
  float diffs = 0;	// Sums difference from mean

  cin >> in;

  while(in != -1){
    sum += in;		// Sum all values in array
    nums[i] = in;	// Set the input, in, to the current array value
    i ++;
    cin >> in;
  }
  
  float average = sum/i;

  // Loop sums the squares of differences from the mean
  for (int j = 0; j < i; j++){		
    diffs += pow(average - nums[j],2);
  }
  float stdev = sqrt(diffs/i);

  cout << "Average: " << average << endl;
  cout << "Sandard deviation: " << stdev << endl;

}
