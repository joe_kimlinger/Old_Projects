// Joe Kimlinger
#include <iostream>
#include <iomanip>
#include "nd_data.h"
using namespace std;

// function prototypes
void record(int);
void atleast(int, int, int);
void few(int, int, int);
void pyears(float, int);
void pc(int);
int games(int);

int main(){
  int choice;
  bool cont = true;
  int currentyear = 2016;
  int year;
  int n;
  float perc;

  // Continues to run until quit is selected
  while(cont){
    cout << endl;
    cout << "Notre Dame Football statistics" << endl;
    cout << "1: Display the record for a given year." << endl;
    cout << "2: Display the years with at least n losses." << endl;
    cout << "3: Display the years with fewer than n losses." << endl;
    cout << "4: Display the years with at least n wins." << endl;
    cout << "5: Display the years with fewer than n wins." << endl;
    cout << "6: Display the years with greater than n win percentage." << endl;
    cout << "7: Display the win percentage for a given year." << endl;
    cout << "8: Display the number of games in a given year." << endl;
    cout << "9: Quit" << endl;
    cin >> choice;

    switch(choice){
      case 1: cout << "Enter the year: ";
  	      cin >> year;
	      record(year);
	      break;
      case 2: cout << "Enter number of losses: ";
	      cin >> n;
	      atleast(n, 0, currentyear);
	      break;
      case 3: cout << "Enter number of losses: ";
	      cin >> n;
	      few(n, 0, currentyear);
	      break;
      case 4: cout << "Enter number of wins: ";
	      cin >> n;
	      atleast(n, 1, currentyear);
	      break;
      case 5: cout << "Enter number of wins: ";
	      cin >> n;
	      few(n, 1, currentyear);
	      break;
      case 6: cout << "Enter the percentage: ";
	      cin >> perc;
	      pyears(perc, currentyear);
	      break;
      case 7: cout << "Enter the year: ";
 	      cin >> year;
	      pc(year);
	      break;
      case 8: cout << "Enter the year: ";
	      cin >> year;
	      cout << "There were " << games(year) << " games in " << year <<endl;
	      break;
      case 9: cont = false;
	      break;
    }  
  }
}

// Function displays the record for a given year
void record(int year){
  int w = wins[year - 1900];
  int l = losses[year - 1900];
  cout << endl;
  cout << "The record in " << year << " was " << w << "-" << l << endl;
}

// Function returns the years with at leat n wins or n losses
void atleast(int n, int r, int currentyear){
  if (r){		// If r is 1, the fucntion runs wins mode
    cout << endl;
    cout << "There were at least " << n << " wins in: " << endl;
    // Find years with at least n wins
    for (int i = 0; i < currentyear - 1900; i++){
      if(wins[i] >= n){
	cout << i + 1900 << " ";
      }
    }
  }
  // Loss mode
  else{
    cout << endl;
    cout << "There were at least " << n << " losses in:" << endl;
    for (int i = 0; i < currentyear - 1900; i++){
      if (losses[i] >= n){
	cout << i + 1900 << " ";
      }
    }
  }
}
 // Function displays years with fewer than n wins or losses
void few(int n, int r, int currentyear){
  // Calculates fewer than n wins
  if (r){
    cout << endl;
    cout << "There were fewer than " << n << " wins in: " << endl;
    for (int i = 0; i < currentyear - 1900; i++){
      if (wins[i] < n){
	cout << i + 1900 << " ";
      }
    }
  }
  // Fewer than n losses
  else{
    cout << endl;
    cout << "There are fewer than " << n << " losses in: " << endl;
    for (int i = 0; i < currentyear - 1900; i++){
      if (losses[i] < n){
	cout << i + 1900 << " ";
      }
    }
  }
}

// Function returns the years over p win percentage
void pyears(float p, int currentyear){
  cout << endl;
  cout << "The years with more than " << p << "% wins are: " << endl;
  for (int i = 0; i < currentyear - 1900; i++){
    if (wins[i]/games(i + 1900)){	// Compute percentage using games() func
      cout << i + 1900 << " ";
    }
  }
}

// Function returns the win percentage for a given year
void pc(int year){
  cout << endl;
  float p = (float)wins[year - 1900] / games(year);
  p *= 100;
  cout << "The win percentage in " << year << " was " << setprecision(3) << p;
  cout << "%" << endl;
}
 
// Computer number of games in a given year
int games(int year){
  return losses[year - 1900] + wins[year - 1900];
}
