// Joe Kimlinger
#include <iostream>
#include <iomanip>
using namespace std;

int main(){
  int n = 1000;
  int p = 2;
  int nums [n];
  int nonprimes = 2;

  // make an array from 2 to n, 0 and 1 are  not prime and won't be included
  for (int i = 1; i <= n - 1; i++){
    nums[i-1] = i + 1;
  }

  // Main loop, executes until only one prime is found 
  while (nonprimes > 1){
   nonprimes = 0; 

   // Loop increments by p each time to find non-primes that are multiples of p
   for (int i = p - 2; i <= n - 2 - p; i+= p){
      if (nums[i + p]){		// nums[i+p] is a multiple of p, so not prime
        nums[i + p] = 0;	// Change all non primes to zeros
        nonprimes += 1;		
      }
    }
    if (nonprimes > 1){
      // Iterate starting from old p to find a new p or step value
      for (int i = p - 1; i < n - 2; i++){
        if (nums[i]){		// Next non-zero number becomes the new p
	  p = nums[i];
  	  break;		// A new p is found, so loop breaks
        }
      }
    }
  }

  bool stop = false; 

  // Print loop prints rows until there array ends
  while (!stop){
    // Runs 10 times for 10 columns
    for (int k = 0; k < 10; k++){
      // Iterate through all values to find primes
      for(int l = 0; l < n - 1; l++){
	if (nums[l]){
	  cout << setw(3) << nums[l] << " ";
	  nums[l] = 0;		// Set current prime as 0 so it will be skipped
	  break;
	}
	// If all items in array are 0, stop printing
	if (l == n - 2){
	  stop = true;
        }
      }
    }
    cout << endl;
  }
}
