#include <iostream>
#include <cmath>
using namespace std;

int main(){
 float sfric;
 float angle;
 float kfric;

 cout << "What is the angle of incline? ";
 cin >> angle;
 angle = angle * 2 * 3.14159 / 360;
 if (angle < 0 || angle > 3.14159/2 + .01){
  cout << "Angle must be between 0 and 90 degrees" << endl;
  return 0;
 }

 cout << "What is the coifficient of static friction? ";
 cin >> sfric;
 if (sfric < 0){
  cout << "Coefficient must be positive" << endl;
  return 0;
 }

 cout << "What is the coefficient of kinetic friction? ";
 cin >> kfric;
 if (kfric < 0){
  cout << "Coefficient must be positive" << endl;
  return 0;
 }
 if (kfric > sfric){
  cout << "The coefficient of kinetic friction cannot be greater than static" << endl;
  return 0;
 }

 float a = sfric*cos(angle) * 9.81 - 9.81 * sin(angle);
 
 if (a >= 0){
  cout << "Block is stationary" << endl;
 } 
 else {
  a = kfric*cos(angle) * 9.81 - 9.81 * sin(angle);
  cout << "The acceleration of the block is " << a << " m/s" << endl;
 }

}
