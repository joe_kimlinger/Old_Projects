// Joe Kimlinger
#include "gfx.h"
#include <cmath>
using namespace std;

void sierpinski(int, int, int, int, int, int);
void triangle(int, int, int, int, int, int);
void shrinking_squares(int, int, int, int, int, int, int, int);
void spiral_squares(int, float, int);
void square(int, int, int, int, int, int, int, int);
void circle_lace(int, int, double);
void snowflake(int, int, double);
void tree(int, int, double, double);
void fern(int, int, double, double);
void spiralspirals(int, int, double, double);
int height = 700;
int width = 700;

int main(){

  gfx_open(width, height, "Fractals");
  gfx_clear();

  char cmd;

  while (1){
    cmd = gfx_wait();
    switch (cmd){
      // Trinalge fractal
      case '1': 
        gfx_clear();
        sierpinski(width/10, height/10, 9*width/10, height/10, width/2, 9*height/10);
        break;
      // Square fractal
      case '2':
        gfx_clear();
        shrinking_squares(width/4, height/4, 3*width/4, height/4, 3*width/4, 3*height/4, width/4, 3*height/4);
        break;
      // Spiraling square fractal
      case '3':
        gfx_clear();
        spiral_squares(sqrt(pow(height/2 - 50,2) + pow(width/2 - 50, 2)), -tan((height/2 - 50)/(width/2 - 50)), 100);
        break;
      // Circle fractal
      case '4':
        gfx_clear();
        circle_lace(width/2, height/2, 225);
        break;
      // Pentagonal snowflake fractal
      case '5':
        gfx_clear();
        snowflake(width/2, height/2, 225);
        break;
      // Tree fractal
      case '6':
        gfx_clear();
        gfx_line(width/2, height - 20, width/2, 2*height/3);
        tree(width/2, 2*height/3, 150, M_PI/2);
        break;
      // fern fractal
      case '7':
        gfx_clear();
        fern(width/2, height - 20, 400, M_PI/2);
        break;
      // Spiral fractal
      case '8':
        gfx_clear();
        spiralspirals(width/2, height/2, 400, 7*M_PI/4);
        break;
      // q to quit
      case 'q':
        return 0;
    }
  }
}

void sierpinski(int x1, int y1, int x2, int y2, int x3, int y3){
  // Base case
  if (abs(x2 - x1) < 4.5) return;

  // Draw triangle
  triangle(x1, y1, x2, y2, x3, y3);

  // Call recursively
  sierpinski(x1, y1, (x1+x2)/2, (y1+y2)/2, (x1+x3)/2, (y1+y3)/2 );
  sierpinski((x1+x2)/2, (y1+y2)/2, x2, y2, (x2+x3)/2, (y2+y3)/2 );
  sierpinski((x1+x3)/2, (y1+y3)/2, (x2+x3)/2, (y2+y3)/2, x3, y3 );
}

// Draw a triangle
void triangle(int x1, int y1, int x2, int y2, int x3, int y3){
  gfx_line(x1, y1, x2, y2);
  gfx_line(x2, y2, x3, y3);
  gfx_line(x3, y3, x1, y1);
}

void shrinking_squares(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4){
  // Base case
  int dist = abs(x1 - x2);
  if (dist < 2) return;

  // Draw square
  square(x1, y1, x2, y2, x3, y3, x4, y4);

  // Recursive calls
  shrinking_squares(x2-dist/4.5, y2-dist/4.5, x2+dist/4.5, y2-dist/4.5, x2+dist/4.5, y2+dist/4.5, x2-dist/4.5, y2+dist/4.5);
  shrinking_squares(x3-dist/4.5, y3-dist/4.5, x3+dist/4.5, y3-dist/4.5, x3+dist/4.5, y3+dist/4.5, x3-dist/4.5, y3+dist/4.5);
  shrinking_squares(x4-dist/4.5, y4-dist/4.5, x4+dist/4.5, y4-dist/4.5, x4+dist/4.5, y4+dist/4.5, x4-dist/4.5, y4+dist/4.5);
  shrinking_squares(x1-dist/4.5, y1-dist/4.5, x1+dist/4.5, y1-dist/4.5, x1+dist/4.5, y1+dist/4.5, x1-dist/4.5, y1+dist/4.5);
}

void spiral_squares(int r, float theta, int sidelen){
  // Base case
  if (sidelen < 2) return;

  int x = width/2 + r*cos(theta);
  int y = height/2 - r*sin(theta);

  // Draw square passing center point - 1/2 * side length
  square(x - sidelen/2, y - sidelen/2, x - sidelen/2, y + sidelen/2, x + sidelen/2, y + sidelen/2, x + sidelen/2, y - sidelen/2);

  spiral_squares(.9 * r, theta - .6, .93 * sidelen);
}

// Simply draws a square
void square(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4){
  gfx_line(x1, y1, x2, y2);
  gfx_line(x2, y2, x3, y3);
  gfx_line(x3, y3, x4, y4);
  gfx_line(x4, y4, x1, y1);
}

void circle_lace(int x, int y, double r){
  // Base case
  if (r < .5) return;

  // Draw circle
  gfx_circle(x, y, r);

  // Call recursively at 6 different points around the circle
  for (double i = 0; i < 2 * M_PI; i += M_PI/3){
    circle_lace(x + r*cos(i), y - r*sin(i), .33*r);
  }
}

void snowflake(int x, int y, double len){
  // Base case
  if (len < 1) return;

  // Draw each line (5 total) and call each line recursively
  for (double theta = -3*M_PI/2; theta < 3*M_PI/2; theta += 2*M_PI/5){
    gfx_line(x, y, x + len * cos(theta), y + len * sin(theta));
    snowflake(x + len*cos(theta), y + len*sin(theta), .35*len);
  }
}

void tree(int x, int y, double len, double theta){
  // Base case
  if (len < 2) return;
  
  // Draw lines coming off previous at 30 degree angles
  gfx_line(x, y, x + len*cos(theta - M_PI/6), y - len*sin(theta - M_PI/6));
  gfx_line(x, y, x + len*cos(theta + M_PI/6), y - len*sin(theta + M_PI/6));

  // Call recursively
  tree(x + len*cos(theta - M_PI/6), y - len*sin(theta - M_PI/6), .7*len, theta - M_PI/6);
  tree(x + len*cos(theta + M_PI/6), y - len*sin(theta + M_PI/6), .7*len, theta + M_PI/6);
}

void fern(int x, int y, double len, double theta){
  // Base case
  if (len < 4) return;

  // One line in the center
  gfx_line(x, y, x + len*cos(theta), y - len*sin(theta));

  // 4 lines at equal intervals on the center line, each coming off at an 
  //   angle of pi/5
  for (int i = 1; i <= 5; i++){
    fern(x + i*len*cos(theta)/5, y - i*len*sin(theta)/5, .35*len, theta - M_PI/5);
    fern(x + i*len*cos(theta)/5, y - i*len*sin(theta)/5, .35*len, theta + M_PI/5);
 }
}

// Draw a point in a spiral formation then draw a spiral centered at each point
void spiralspirals(int x, int y, double r, double theta){
  // Base case
  if (r < 1) return;

  double tr = r, tt = theta;	// Assign temporary variables for r and theta
  // Draw a decreasnig spiral
  while (tr > 2){
    gfx_point(x + tr*cos(tt), y - tr*sin(tt));
    // Pass each point recursively
    spiralspirals(x + tr*cos(tt), y - tr*sin(tt), .35*tr, tt);
    tr = .9*tr;
    tt -= M_PI/5;
  }
}
