#include <iostream>
#include <iomanip>
using namespace std;

int main(){
 float bal;
 float total = 0.00;
 float rate;
 float intr;
 int monthwidth;
 int month = 0;
 float monthly;

 cout << "Principal: ";
 cin >> bal;

 cout << "Interest Rate: ";
 cin >> rate;
 if (rate <= 0.0){
   cout << "Please enter a positive interest rate." << endl;
   return 0;
 }

 cout << "Desired Monthly Payment: ";
 cin >> monthly;

 if (bal - monthly + bal*rate/12 >= bal){
   cout << "Your monthly payment is not enough for this interest rate." << endl;
   return 0;
 }

 cout << setw(10) << "Month      Payment       Interest    Balance" << 
 endl;

 while (bal >0){
   intr = bal * rate/12;
   if (bal - monthly + intr >= 0)
     bal = bal - monthly + intr;
   else {
     monthly = bal;
     bal = 0.00;
   }
   month++;

   if (month < 10) monthwidth = 2;
   else if (month < 100) monthwidth = 1;
   else if (month < 1000) monthwidth = 0;
   else if (month < 10000) monthwidth = -1;
   else monthwidth = -2;

   cout << month << setw(14+monthwidth) << fixed << setprecision(2) << monthly
   << setprecision(2)<< setw(14) << fixed << intr << setw(14)<< 
   setprecision(2) << bal << endl;

    total += monthly;
 }
 int years = month / 12;
 month %= 12;
 cout << "You paid a total of $" << total << " over " << years << " years and "
 << month << " months." << endl;

}
