#include <iostream>
#include <iomanip>
using namespace std;

int main(){
 int numCols;
 int numRows;
 int x;
 int y;
 int setCols;
 
 cout << "Rows: ";
 cin >> numRows;
 cout << "Columns: ";
 cin >> numCols;

 cout << setw(3) << "*";
 for (setCols=1; setCols <= numCols; setCols++){
  cout << setw(3) << setCols << " ";
 }
 cout << endl;
 for (x=1; x <= numRows; x++){
  cout << setw(3) << x;

  for (y=1; y <= numCols; y++){
    cout << setw(3) << x*y << " ";
  }
  cout << endl;

 }

}
