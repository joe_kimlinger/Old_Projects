#include <iostream>
#include <iomanip>
#include <math.h>
using namespace std;

int main(){
  float x;
  float dx = .03;
  float y;
  float max = 1.62;
  float min = 1.62;
  float minspot;
  float maxspot;
  float i;

  cout << "Plotting y = e^(-x)*cos(2*pi*x) from x = 0 to x = 4.0" << endl;
  cout << setw(5) << "X" << setw(5) << "Y" << endl;

  for (x = 0; x <=4.0; x+=dx){
    y = exp(-x)*cos(2*3.1415926*x) + .62;
    
    if (y > max){
      max = y;
      maxspot = x;
    }
    if (y < min){
      min = y;
      minspot = x;
    }

    cout << setw(6) << fixed << setprecision(2) << x << setw(6) << y << " ";

    for ( i = 0; i <= y; i+= .03){
      cout << 'X';
    }
    cout << endl;
  }
  
  cout << "The maximum of " << max << " was at " << maxspot << endl;
  cout << "The minimum of " << min << " was at " << minspot << endl;
}
