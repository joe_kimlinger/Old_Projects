#include <iostream>
#include <iostream>
using namespace std;

int getgcd(int, int);

int main(){
	int a, b, gcd;
	
	cout << "Enter two integers: ";
	cin >> a >> b;
	
	gcd = getgcd(a,b);
	cout << "The greatest common denominator is: " << gcd << endl;
}

int getgcd(int a,int b){
	int gcd;
	for (int i = 1; i <=a; i++){
		if (a%i == 0 && b%i == 0){
			gcd = i;
		}
	}
	return gcd;
}
