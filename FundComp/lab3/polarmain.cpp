#include "polarfn.h"
#include <iostream>
#include <iomanip>
#include <cmath>

int main(){
	double x, y;
	double ang, r;
	int quad;
	
	cout << "Enter x coordinate: ";
	cin >> x;
	
	cout << "Enter y coordiante: ";
	cin >> y;

	// Calculate values using fucntions
	quad = quadrant(x,y);
	r = pradius(x,y);
	ang = pangle(x,y);
	
	// Check for discrepencies that occur using the inverse tan funciton
	if (quad == 3){ ang = -ang - 90;}
	if (quad == 2){ ang = -ang + 90;}
	
	cout << "The polar coordinates are: (" << setprecision(5) << r << ",";
	cout << ang << ")" << endl;
	
	// quad: 1-4 correspont to qudrants while 5 and 6 are the y and x axis
	if (quad >= 1 && quad <= 4){
		cout << "The point is in quadrant: " << quad << endl;
	}
	else if (quad == 5){cout << "The point lies on the y axis" << endl;}
	else if (quad == 6){cout << "The point lies on the x axis" << endl;}
	else{ cout << "The point is at the origin" << endl;}
}
