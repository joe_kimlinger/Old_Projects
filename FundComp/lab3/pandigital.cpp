// Joe Kimlinger
#include <iostream>
#include <iomanip>
#include <cmath>
using namespace std;


int main(){
  int number = 0;
  int n;
  bool prime;

  cout << "Input n to find the largest n digit prime pandigital number: ";
  cin >> n;

  for (int i = 1; i <= n; i ++){
    number += i * pow(10,i-1);
  }
  for (int j = 2; j < number/2; j++){
    if (number % j == 0){
      prime = false;
      break;
    }
    else{
      prime = true;
    }
  }

  cout << number << endl;
  cout << prime << endl;
}
