// Joe Kimlinger
#include <iostream>
#include <iomanip>
#include <cmath>
using namespace std;

double pradius(double, double);
double pangle(double, double);
int quadrant(double, double);

int main(){
	double x, y;
	double ang, r;
	int quad;
	
	cout << "Enter x coordinate: ";
	cin >> x;
	
	cout << "Enter y coordiante: ";
	cin >> y;

	// Calculate values using fucntions
	quad = quadrant(x,y);
	r = pradius(x,y);
	ang = pangle(x,y);
	
	// Check for discrepencies that occur using the inverse tan funciton
	if (quad == 3){ ang = -ang - 90;}
	if (quad == 2){ ang = -ang + 90;}
	
	cout << "The polar coordinates are: (" << setprecision(5) << r << ",";
	cout << ang << ")" << endl;
	
	// quad: 1-4 correspont to qudrants while 5 and 6 are the y and x axis
	if (quad >= 1 && quad <= 4){
		cout << "The point is in quadrant: " << quad << endl;
	}
	else if (quad == 5){cout << "The point lies on the y axis" << endl;}
	else if (quad == 6){cout << "The point lies on the x axis" << endl;}
	else{ cout << "The point is at the origin" << endl;}
}

double pradius(double x, double y){
	double r = sqrt(pow(x,2) + pow(y,2));
	return r;
}

// pangle takes extra steps to ensure no dividing by 0
double pangle(double x, double y){
	double ang;
	if (x == 0){
		if (y >= 0){ ang = 90;}
		else {ang = -90;}
	}
	if (y == 0){
		if (x < 0){ang = 180;}
		else {ang = 0;}
	}
	else {
		ang = atan(y/x)*180/M_PI;
	}

	return ang;
}

// Returns 1-4 for quadrants and 5 and 6 for y and x axes
int quadrant(double x, double y){
	if (x == 0){
		if (y == 0){ return 0; }
		else return 5;
	}
	else if (y == 0){ return 6; }

	else if (y > 0){
		if (x > 0){ return 1; }
		else { return 2; }
	}
	else{
		if (x < 0){ return 3; }
		else {return 4; }
	}
}
