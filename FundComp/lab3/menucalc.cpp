// Joe Kimlinger 9/14/16

#include <iostream>
#include <iomanip>
using namespace std;

void add(double, double);
void subtract(double, double);
void multiply(double, double);
void divide(double, double);

int main(){
	float x, y;
	bool run = true;	// Conditional for main while loop
	int menu = 0;
	
	// Main while loop, runs until quit is selected
	while (run){
		// Display menu
		cout << "Select an option: \n";
		cout << "(1) Addition \n" << "(2) Subtraction \n";
		cout << "(3) Multiplication \n" << "(4) Division \n";
		cout << "(5) Quit \n" << "Option: ";
		cin >> menu;

		// If quit is not selected, input numbers
		if (menu < 5){
			cout << "Enter two numbers: ";
			cin >> x >> y;
		}
		// If quit, exit while loop immediately
		else{
			cout << "Thanks for playing! \n";
			break;
		}
		
		// Each option runs a function then exits the switch statement
		switch(menu){
			case 1: add(x, y);
					break;
			case 2: subtract(x, y);
					break;
			case 3: multiply(x, y);
					break;
			case 4: divide(x, y);
					break;
		}
	}
}

// All four operation functions compute then display result
void add(double x, double y){
	double ans = x + y;
	cout << "Your equation is: " << x << " + " << y << " = "<< ans << endl;
}

void subtract(double x, double y){
	double ans = x - y;
	cout << "Your equation is: " << x << " - " << y << " = "<< ans << endl;
}

void multiply(double x, double y){
	double ans = x * y;
	cout << "Your equation is: " << x << " x " << y << " = "<< ans << endl;
}

void divide(double x, double y){
	double ans = x / y;
	cout << "Your equation is: " << x << " / " << y << " = "<< ans << endl;
}
