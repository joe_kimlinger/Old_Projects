#include <cmath>
double pradius(double x, double y){
	double r = sqrt(pow(x,2) + pow(y,2));
	return r;
}

// pangle takes extra steps to ensure no dividing by 0
double pangle(double x, double y){
	double ang;
	if (x == 0){
		if (y >= 0){ ang = 90;}
		else {ang = -90;}
	}
	if (y == 0){
		if (x < 0){ang = 180;}
		else {ang = 0;}
	}
	else {
		ang = atan(y/x)*180/M_PI;
	}

	return ang;
}

// Returns 1-4 for quadrants and 5 and 6 for y and x axes
int quadrant(double x, double y){
	if (x == 0){
		if (y == 0){ return 0; }
		else return 5;
	}
	else if (y == 0){ return 6; }

	else if (y > 0){
		if (x > 0){ return 1; }
		else { return 2; }
	}
	else{
		if (x < 0){ return 3; }
		else {return 4; }
	}
}
