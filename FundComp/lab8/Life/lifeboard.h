// Joe Kimlinger
#include <iostream>
#include <vector>
using namespace std;


class Board{
  public:
    Board();
    void setBoard(int, int);
    ~Board();
    void setValue(int, int, char);
    char getValue(int, int);
    void update();
    void print();
  private:
    int rows;
    int cols;
    char board[40][40];

};
