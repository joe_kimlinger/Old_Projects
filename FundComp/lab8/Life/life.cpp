// Joe Kimlinger
#include <iostream>
#include <unistd.h>
#include <cstdlib>
ony-fixed-medium-r-normal--24-170-100-100-c-120-jisx0201.1976-0include "lifeboard.h"
#include <fstream>
using namespace std;

void greeting();

int main(int argc, char *argv[]){
  Board a;
  char choice;
  int row;
  int col;
  char val;
  fstream file;
  bool start = true;
  bool notp = true;

  // Loop runs until a p is read, etiher in a file or cmd line
  while (notp){

    // Interactive mode if there are no cmd line arguments
    if (argc == 1){
      // Greeting and instructions only run upon startup
      if (start){
        greeting();
        start = false;
      }
      a.print();
      cout << "Command: ";
      cin >> choice;
      // input location to add live cell, value of live cell will be an X
      if (choice == 'a'){
        val = 'X';
        cin >> row;
        cin >> col;
      }
      // Input coordinates to kill a cell, space represents dead cell
      if (choice == 'r'){
        val = ' ';
        cin >> row;
        cin >> col;
      }
      // "help" command displays instructions again
      if (choice == 'h') greeting();
    }

    // Batch mode if one command line argument
    else if (argc == 2){
      // Runs once to open file
      if (start){
        file.open(argv[1]);
        start = false;
      }
      a.print();
      file >> choice;
      // a adds a cell, read row and column to know where
      if (choice == 'a'){
        val = 'X';
        file >> row;
        file >> col;
      }
      // r kills cell at desired coordinate
      if (choice == 'r'){
        val = ' ';
        file >> row;
        file >> col;
      }
    }
   

    // Actions performed based on command 
    // setValue sets either 'X' or ' ' based on val, which is based on a or r
    if (choice == 'a' || choice == 'r'){
      a.setValue(row, col,  val);
    }
    // Run through one round of Conway's game rules
    if (choice == 'n') a.update();
    // Quit
    if (choice == 'q') return 0;
    // Exit while loop to continue to continious loop
    if (choice == 'p') notp = false;
    system("clear");
  }

  // Continious mode
  while (true){
    system("clear");
    a.update();
    a.print();
    usleep(200000);	// To slow down simulation
  }
}

void greeting(){
  char start;
  cout << "Welcome to interactive mode!\n";
  cout << "How to play:\nEnter \"a row column\" to add a live cell\n";
  cout << "Enter \"r row column\" to kill a cell\n";
  cout << "Enter \"n\" to advance through one scene\nEnter q to quit\n";
  cout << "Enter \"p\" to run the simulation continuously\n";
  cout << "Type \"ctrl + C\" to end simulation.\nEnter \"h\" for help\n";
  cout << "Let's begin, type s to get started!: ";
  // Waits till only s is pressed
  while (start != 's'){
    cin >> start;
  }
}
