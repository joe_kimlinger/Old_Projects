// Joe Kimlinger
#include "lifeboard.h"
#include <iomanip>
#include <iostream>
using namespace std;

// Initializer
Board::Board(){
  setBoard(40,40);
  // Set all values to dead, blank space
  for (int i = 0; i < rows; i ++){
    for (int j = 0; j < cols; j++){
      board[i][j] = ' ';

    }
  }
}

void Board::setBoard(int r, int c){
  rows = r;
  cols = c;
}

Board::~Board(){}

void Board::setValue(int row, int col, char v){
  board[row][col] = v;
}

char Board::getValue(int row, int col){
  return board[row][col];
}

// Runs board thorugh one round of the game rules
void Board::update(){
  Board n;		// New board will be returned as updated board
  int liveNeburs;	// Number of alive surrounding cells for each spot
  for (int i = 0; i < rows; i++){
    for (int j = 0; j < cols; j++){
      liveNeburs = 0;	// Set to zero and increment for each spot on board
      // Following if statements account for borders
      // Will not try to index if value is outside of array
      if (getValue(i - 1,j) == 'X' && i != 0) liveNeburs++;
      if (getValue(i - 1,j - 1) == 'X' && i != 0 && j != 0) liveNeburs++;
      if (getValue(i + 1,j) == 'X' && i != rows - 1) liveNeburs++;
      if (getValue(i + 1,j - 1) == 'X' && i != rows - 1 && j != 0) liveNeburs++;
      if (getValue(i,j - 1) == 'X' && j != 0) liveNeburs++;
      if (getValue(i - 1,j + 1) == 'X' && i != 0 && j != cols - 1) liveNeburs++;
      if (getValue(i,j + 1) == 'X' && j != cols - 1) liveNeburs++;
      if (getValue(i+1,j+1) == 'X' && i != rows - 1 && j != cols - 1) liveNeburs++;
      // Keep live cell alive if surrounded by 2 live cells and make cell
      // live if surrounded by 3 live cells
      if (liveNeburs == 3 || (liveNeburs == 2 && getValue(i,j) == 'X'))
         n.setValue(i,j,'X');
    }
  }
  // Update board to be equal to new updated board
  for (int i = 0; i < rows; i++){
    for (int j = 0; j < cols; j++){
      setValue(i, j, n.getValue(i,j));
    }
  }
}

void Board::print(){
  // Set top border
  for (int i = 0; i <= cols + 1; i++){
    cout << '*';
  }
  cout << endl;
  // cout * for border then full row then * for right border
  for(int i = 0; i < rows; i++){
    cout << '*';
    for (int j = 0; j < cols; j++){
      cout << board[i][j];
    }
    cout << '*' << endl;
  }
  // Bottom border
  for (int i = 0; i <= cols + 1; i++){
    cout << '*';
  }
  cout << endl;
}
