// Joe Kimlinger
#include <iostream>
#include <iomanip>
#include "rational.h"
using namespace std;


Rational::Rational(){
  setRational(1, 1);
}

Rational::Rational(int x, int y){
  setRational(x, y);
}

Rational::~Rational(){}

void Rational::setRational(int n, int d){
  numer = n;
  denom = d;
}

int Rational::getNumer(){
  return numer;
}

int Rational::getDenom(){
  return denom;
}

void Rational::setNumer(int x){
  numer = x;
}

void Rational::setDenom(int y){
  denom = y;
}


Rational Rational::operator+(Rational x){
  Rational s;
  s.setRational(x.denom * numer + x.numer * denom, x.denom * denom);
  return s;
}

Rational Rational::operator-(Rational x){
  Rational d;
  d.setRational(x.denom * numer - x.numer * denom, d.denom = denom * x.denom);
  return d;
}

Rational Rational::operator*(Rational x){
  Rational p;
  p.setRational(numer * x.numer, p. denom = denom * x.denom);
  return p;
}

Rational Rational::operator/(Rational x){
  Rational q;
  q.setRational(numer * x.denom, q.denom = denom * x.numer);
  return q;
}

void Rational::reduce(){
  int a, b, gcd;
  // find largest number and set as a, set smaller as b
  if (numer > denom){
    a = numer;
    b = denom;
  }
  else{
    a = denom;
    b = numer;
  }

  // Find the gcd of the numer and denom
  for (int i = 1; i <= b; i++){
    if (a % i == 0 && b % i == 0) gcd = i;
  }

  numer /= gcd;
  denom /= gcd;
}

ostream& operator<< (ostream& s, Rational& x){
  x.reduce();
  s << x.getNumer() << '/' << x.getDenom();
  return s;
}

istream& operator>> (istream& s, Rational& x){
  int a, b;
  s >> a >> b;
  x.setRational(a, b);

  return s;
}
