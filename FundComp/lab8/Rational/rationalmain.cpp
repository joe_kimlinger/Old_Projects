// Joe Kimlinger
#include <iostream>
#include <iomanip>
#include "rational.h"
using namespace std;


int main(){

  // r is used to perform operation
  Rational a, b, r;
  a.setRational(5,6);
  b.setRational(1,2);

  // Display a
  cout << "a is " << a << endl;

  // Display b
  cout << "b is " << b << endl;

  // a + b
  r = a + b;
  cout << "a + b = " << r << endl;

  // a - b
  r = a - b;
  cout << "a - b = " << r << endl;

  // a * b
  r = a * b;
  cout << "a * b = " << r << endl;

  // a / b
  r = a / b;
  cout << "a / b = " << r << endl;

}
