// Joe Kimlinger
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <fstream>
using namespace std;

void displayall(vector<string>&);
void addsaying(string, vector<string>&);
void findword(string, vector<string>&);
void writeout(string, vector<string>&);

int main(){
  // Initiaize variables used throughout main
  string datafile;
  vector<string> sayings;
  int choice;
  string s;
  string newsaying;
  bool game = true;
  string word;
  string newfile;

  // Ask for file name from user and create stream from file
  cout << "Enter a startup data file: ";
  cin >> datafile;
  fstream file(datafile);

  // If file doesn't exist, end the program
  if(!file){
    cout << "Error opening file." << endl;
    return 0;
  }
  
  // Store each line of the file as an element in the vector sayings 
  getline(file, s);
  while(!file.eof()){
    sayings.push_back(s);
    getline(file, s);
  }
  sayings.pop_back();	// Delete extra line at end of file
  
  // Runs until quit is selected
  while(game){
    // Options menu
    cout << "\n(1) Display all sayings \n";
    cout << "(2) Enter a new saying \n";
    cout << "(3) List sayings that contain a given word \n";
    cout << "(4) Save sayings \n";
    cout << "(5) Quit \n";
    cout << "What would you like to do? ";
    cin >> choice;

    switch(choice){
      case 1: 
	  displayall(sayings);
	  break;
      case 2:
	  cout << "Enter a saying: ";
	  cin.ignore();			// Ignore endl characted stored in cin
	  getline(cin, newsaying);
	  addsaying(newsaying, sayings);
	  break;
      case 3:
	  cout << "Enter a word: ";
	  cin.ignore();			// Ignore endl char from cin
	  getline(cin, word);
	  findword(word, sayings);
	  break;
      case 4:
	  cout << "Save as: ";
	  cin >> newfile;
	  writeout(newfile, sayings);
	  break;
      case 5:
	  game = false;
	  break;
    }
  }
}

// Displays all sayings in vector sayings
void displayall(vector<string>& sayings){

  for (auto it = sayings.begin(); it != sayings.end(); it++){
    cout << *it << endl;
  }
}

// Adds a saying to the end od the vector sayings
void addsaying(string newsaying, vector<string>& sayings){
  sayings.push_back(newsaying);
}

// Searches for a word in each saying
void findword(string word, vector<string>& sayings){
  // Loop iterates through each saying in sayings
  for (auto it : sayings){
    // If word is found, saying is displayed 
    if (it.find(word) != -1){
      cout << it << endl;
    } 
  }
}

// Saves current sayings vector to specified file name
void writeout(string datafile, vector<string>& sayings){
  // Add element at the end of sayings to account for pop_back in start of program
  sayings.push_back(" ");	

  ofstream file(datafile);

  for (auto it = sayings.begin(); it != sayings.end(); it++)
    file << *it << "\n";

  // Delete empty space added in start of function
  sayings.pop_back();
}
