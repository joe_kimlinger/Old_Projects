// Joe Kimlinger
#include <iostream>
#include <iomanip>
#include "state.h"
#include <vector>
#include <fstream>
using namespace std;

void displayall(vector<State>&);
void displaystate(vector<State>&, string);
vector<State> tops(vector<State>&, int, char);

int main(){
  string s;
  bool game = true;
  string st1;
  State c;
  int choice;
  int n;
  string filename;
  vector<State> nstates;		// Used for options 3 4 & 5
  vector<State> states;

  cout << "Enter file name: ";
  cin >> filename;
  fstream file(filename);

  if (!file){
    cout << "Error opening file.\n";
    return 0;
  }

  getline(file, s, ',');	// Get abbreviation
  while (!file.eof()){
    c.setAbbrev(s);
    getline(file, s, ',');	// Get name
    c.setName(s);
    getline(file, s, ',');	// Get Capital
    c.setCapital(s);
    getline(file, s, ','); 	// Get Population
    c.setPop(stoi(s));
    getline(file, s, ',');	// Get founded year
    c.setFounded(stoi(s));
    getline(file, s);		// Get representatives
    c.setReps(stoi(s));
  
    states.push_back(c);	// Add class to array
    getline(file, s, ',');
  }

  while(game){
    // Menu
    cout << "Welcome.\n Choose and option:\n";
    cout << "(1) Display all 50 states\n";
    cout << "(2) Display info for a single state\n";
    cout << "(3) Display top n states with the largest population\n";
    cout << "(4) Display top n states with most representatives in congress\n";
    cout << "(5) Display first n states founded\n";
    cout << "(6) Quit\n";
    cout << "Option: ";
    cin >> choice;

    switch(choice){
      case 1:
	displayall(states);
	break;
      case 2:
	cout << "Enter state abbreviation: ";
	cin >> st1;
	// for loop accounts for lowercase values
	for (auto it = st1.begin(); it != st1.end(); it++){
	  *it = toupper(*it);
	}
	cout << endl;
	displaystate(states, st1);
	break;
      case 3:
	cout << "What number of states: ";
	cin >> n;
	while (n > 50){
	  cout << "Enter a number less than 50: ";
	  cin >> n;
	}
	cout << endl;
	nstates = tops(states, n, 'p');	   // Add the top n populations to nstates
	displayall(nstates);		   // Display all the states in nstates
	break;
      case 4:
	cout << "What number of states: ";
	cin >> n;
	while (n > 50){
	  cout << "Enter a number less than 50: ";
	  cin >> n;
	}
	cout << endl;
	nstates = tops(states, n, 'r');    // Add top n representatives to nstates
	displayall(nstates);		   // Display all states in nstates
	break;
      case 5:
	cout << "What number of states: ";
	cin >> n;
	while (n > 50){
	  cout << "Enter a number less than 50: ";
	  cin >> n;
	}
	cout << endl;
	nstates = tops(states, n, 'f');   // Add first n founded states to nstates
	displayall(nstates);		  // Display nstates
	break;
      case 6:
	game = false;
    }
  }

}

// Displays all elements of given array of class State
void displayall(vector<State>& states){
  State c;
  for (auto it = states.begin(); it != states.end(); it++){
    c = *it;
    c.print();		// Use State print method
    cout << endl;
  }
}

// Display state with given abbreviation
void displaystate(vector<State>& states, string abr){
  State c;
  for (auto it = states.begin(); it != states.end(); it++){
    c = *it;
    // If input matches abbreviation print state with State print method
    if (abr == c.getAbbrev()){		
      c.print();
      cout << endl;
    }
  }
}

// Returns the top n states in a given category
vector<State> tops(vector<State>& states, int n, char type){
  State c;
  int val;
  vector<State> maxstates;
  vector<State> states2 = states;	// In order to prevent ruining states 

  // Find the max n number of times
  for (int i = 1; i <= n; i++){
    // Set val to either a min or a max based on category
    (type == 'f') ? val = 2016 : val = 0; 
    vector<State>::iterator jt = states2.begin();
    for (auto it = states2.begin(); it != states2.end(); it++){
      c = *it;
      // Find state that meets requirements for give category
      // Then reset the value till the whole list is iterated through
      if (c.getPop() > val && type == 'p'){
	val = c.getPop();
	jt = it;
      }
      if (c.getReps() > val && type == 'r'){
	val = c.getReps();
	jt = it;
      }
      if (c.getFounded() < val && type == 'f'){
	val = c.getFounded();
	jt = it;
      }
    }
    // Add max/min to maxstates and delete from states2 list so it is not 
    // counted again
    maxstates.push_back(*jt);
    states2.erase(jt);
  }
  // Return maxstates when n iterations are completed
  return maxstates;
}
