#include <iostream>
#include <iomanip>
#include <string>
#include "state.h"
using namespace std;


State::State(){}

State::~State(){}

string State::getName(){
  return name;
}

string State::getAbbrev(){
  return abbrev;
}

string State::getCapital(){
  return capital;
}

int State::getPop(){
  return pop;
}

int State::getFounded(){
  return founded;
}

int State::getReps(){
  return reps;
}

void State::setName(string n){
  name = n;
}

void State::setAbbrev(string a){
  abbrev = a;
}

void State::setCapital(string c){
  capital = c;
}

void State::setPop(int p){
  pop = p;
}

void State::setFounded(int f){
  founded = f;
}

void State::setReps(int r){
  reps = r;
}

void State::print(){
  cout << "Name: " << name << endl;
  cout << "Abbreviatioin: " << abbrev << endl;
  cout << "Capital: " << capital << endl;
  cout << "Population: " << pop << endl;
  cout << "Founded in: " << founded << endl;
  cout << "Representatives in congress: " << reps << endl;
}
