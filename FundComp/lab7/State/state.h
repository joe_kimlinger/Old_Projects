// Joe Kimlinger
#include <iostream>
#include <string>
#include <iomanip>
using namespace std;


class State{
  public:
    State();
    ~State();
    string getName();
    string getAbbrev();
    string getCapital();
    int getPop();
    int getFounded();
    int getReps();
    void setName(string);
    void setAbbrev(string);
    void setCapital(string);
    void setPop(int);
    void setFounded(int);
    void setReps(int);
    void print();
  private:
    string name;
    string abbrev;
    string capital;
    int pop;
    int founded;
    int reps;
};
