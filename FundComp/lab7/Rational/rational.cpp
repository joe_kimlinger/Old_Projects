// Joe Kimlinger
#include <iostream>
#include <iomanip>
#include "rational.h"
using namespace std;


Rational::Rational(){
  numer = 1;
  denom = 1;
}

Rational::Rational(int x, int y){
  numer = x;
  denom = y;
}

Rational::~Rational(){}

int Rational::getNumer(){
  return numer;
}

int Rational::getDenom(){
  return denom;
}

void Rational::setNumer(int x){
  numer = x;
}

void Rational::setDenom(int y){
  denom = y;
}

void Rational::print(){
  cout << numer << "/" << denom;
}

Rational Rational::add(Rational x){
  Rational s;
  s.numer = x.denom * numer + x.numer * denom;
  s.denom = x.denom * denom;
  return s;
}

Rational Rational::subtract(Rational x){
  Rational d;
  d.numer = x.denom * numer - x.numer * denom;
  d.denom = denom * x.denom;
  return d;
}

Rational Rational:: multiply(Rational x){
  Rational p;
  p.numer = numer * x.numer;
  p. denom = denom * x.denom;
  return p;
}

Rational Rational::divide(Rational x){
  Rational q;
  q.numer = numer * x.denom;
  q.denom = denom * x.numer;
  return q;
}

void Rational::reduce(){
  int a, b, gcd;
  // find largest number and set as a, set smaller as b
  if (numer > denom){
    a = numer;
    b = denom;
  }
  else{
    a = denom;
    b = numer;
  }

  // Find the gcd of the numer and denom
  for (int i = 1; i <= b; i++){
    if (a % i == 0 && b % i == 0) gcd = i;
  }

  numer /= gcd;
  denom /= gcd;
}
