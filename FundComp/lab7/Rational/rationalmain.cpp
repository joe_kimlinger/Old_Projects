// Joe Kimlinger
#include <iostream>
#include <iomanip>
#include "rational.h"
using namespace std;


int main(){

  // r is used to perform operation
  Rational a(5,6);
  Rational b(1,2);
  Rational r;

  // Display a
  cout << "a is ";
  a.print();
  cout << endl;

  // Display b
  cout << "b is ";
  b.print();
  cout << endl;

  // a + b
  r = a.add(b);
  cout << "a + b = ";
  r.reduce();
  r.print();
  cout << endl;

  // a - b
  r = a.subtract(b);
  cout << "a - b = " ;
  r.reduce();
  r.print();
  cout << endl;

  // a * b
  r = a.multiply(b);
  cout << "a * b = ";
  r.reduce();
  r.print();
  cout << endl;

  // a / b
  r = a.divide(b);
  cout << "a / b = ";
  r.reduce();
  r.print();
  cout << endl;

}
