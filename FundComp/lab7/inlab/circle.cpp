#include <iostream>
#include <cmath>
#include "circle.h"
using namespace std;


Circle::Circle(){
      radius = 1;
    }

Circle::Circle(float x){
      radius = x;
    }

Circle::~Circle(){}

float Circle::getRadius(){
      return radius;
    }

void Circle::setRadius(float x){
      radius = x;
    }

float Circle::circumference(){
      return 2 * radius * M_PI;
    }

float Circle::area(){
      return M_PI * pow(radius, 2);
    }
