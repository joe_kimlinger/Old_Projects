// Joe Kimlinger
#include <iostream>
#include <iomanip>
#include <string>
#include <list>
#include <cctype>
#include <fstream>
using namespace std;

void displayall(list<string>&);
void addsaying(string, list<string>&);
void findword(string, list<string>&);
void writeout(string, list<string>&);
void rem(int, list<string>&);

int main(){
  // Initiaize variables used throughout main
  string datafile;
  list<string> sayings;
  int choice;
  string s;
  string newsaying;
  bool game = true;
  string word;
  string newfile;
  bool inserted = false;
  int idel;

  // Ask for file name from user and create stream from file
  cout << "Enter a startup data file: ";
  cin >> datafile;
  fstream file(datafile);

  // If file doesn't exist, end the program
  if(!file){
    cout << "Error opening file." << endl;
    return 0;
  }
  
  // Store each line of the file as an element in the list sayings 
  getline(file, s);
  while(!file.eof()){
    for(auto it = sayings.begin(); it != sayings.end(); it++){
      // Compare the saying to each element in sayings
      if(s.compare(*it) < 0){
        sayings.insert(it, s);	// Instert element in its place alphabetically
	inserted = true;
	break;
      }
    }
    // If saying was not inserted then it comes after all elements in the list
    if (!inserted) sayings.insert(sayings.end(),s);
    getline(file, s);
    inserted = false;
  }
	
  // Delete extra line which is placed at beginning of file alpabetically
  sayings.pop_front();
  
  // Runs until quit is selected
  while(game){
    // Options menu
    cout << "\n(1) Display all sayings \n";
    cout << "(2) Enter a new saying \n";
    cout << "(3) Delete a saying \n";
    cout << "(4) List sayings that contain a given word \n";
    cout << "(5) Save sayings \n";
    cout << "(6) Quit \n";
    cout << "What would you like to do? ";
    cin >> choice;

    switch(choice){
      case 1: 
	  displayall(sayings);
	  break;
      case 2:
	  cout << "Enter a saying: ";
	  cin.ignore();			// Ignore endl characted stored in cin
	  getline(cin, newsaying);
	  addsaying(newsaying, sayings);
	  break;
      case 3:
	  displayall(sayings);
	  cout << "Which saying would you like to delete? ";
	  cin >> idel;
	  rem(idel, sayings);
	  break;
      case 4:
	  cout << "Enter a word: ";
	  cin.ignore();			// Ignore endl char from cin
	  getline(cin, word);
	  cout << "The sayings containing " << word << " are: \n";
	  findword(word, sayings);
	  break;
      case 5:
	  cout << "Save as: ";
	  cin >> newfile;
	  writeout(newfile, sayings);
	  break;
      case 6:
	  game = false;
	  break;
    }
  }
}

// Displays all sayings in vector sayings
void displayall(list<string>& sayings){
  int i = 1;
  for (auto it = sayings.begin(); it != sayings.end(); it++){
    cout << setw(2) << i << ": " << *it << endl;
    i++;
  }
}

// Adds a saying to the end od the vector sayings
void addsaying(string newsaying, list<string>& sayings){
  bool inserted = false;

  // Accounts for uppercase letters
  for (int i = 0; i < newsaying.size(); i++){
    newsaying[i] = tolower(newsaying[i]);
  }

  // Just as when the list was initialized, the new string is compared to all
  // others and inserted at the approprite spot
  for(auto it = sayings.begin(); it != sayings.end(); it++){
      if(newsaying.compare(*it) < 0){
        sayings.insert(it, newsaying);
	inserted = true;
	break;
      }
    }
    if (!inserted) sayings.insert(sayings.end(),newsaying);
    inserted = false;
}

void rem(int i, list<string>& sayings){
  list<string>::iterator it = sayings.begin();
  advance(it, i-1);	// We are already at 1, so we advance i-1
  sayings.erase(it);
}

// Searches for a word in each saying
void findword(string word, list<string>& sayings){
  // Loop iterates through each saying in sayings
  for (auto it : sayings){
    // If word is found, saying is displayed 
    if (it.find(word) != -1){
      cout << it << endl;
    } 
  }
}

// Saves current sayings vector to specified file name
void writeout(string datafile, list<string>& sayings){
  // Add element at the end of sayings to account for pop_back in start of program
  sayings.push_back(" ");	

  ofstream file(datafile);

  for (auto it = sayings.begin(); it != sayings.end(); it++)
    file << *it << "\n";

  // Delete empty space added in start of function
  sayings.pop_back();
}
