// Joe Kimlinger
// counts frequencies of letters in given file
#include <iostream>
#include <iomanip>
#include <cctype>
#include <array>
#include <string>
#include <fstream>
using namespace std;

int main(){
  // Initialize counters
  int spaces = 0;
  int chars = 0;
  int lets = 0;

  int letters[26] = {0};
  string filename;
  char c;

  // Ask for file name
  cout << "File name: ";
  cin >> filename;
  fstream file(filename);

  // If file does not exist, quit program
  if (!file){
    cout << "Error opening file.\n";
    return 0;
  }

  // While loops runs for every char in file
  file.get(c);
  while (!file.eof()){
    c = tolower(c);		// Account for case
    chars++;			// Totsl character counter
    if (isspace(c)) spaces++;
    if (isalpha(c)){	// Only letters of teh alphabet
      letters[c - 97]++;
      lets++;
    }
    file.get(c);
  }
  
  // Print each element of letters array along with its corresponding char
  for (int i = 0; i < 26; i++)
    cout << (char)(i + 97) << ": " << letters[i] << endl;

  cout << "There were " << lets << " letters. \n";
  cout << "There were " << chars << " total characters. \n";
  cout << "There were " << spaces << " total white space characters. \n";
  cout << "Space percentage: ";
  cout << setprecision(3) << (float)100*spaces/chars << "%\n";
}
