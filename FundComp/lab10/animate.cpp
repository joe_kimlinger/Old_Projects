// JOe Kimlinger
#include "gfx.h"
#include <cmath>
#include <unistd.h>
using namespace std;

void grass(int,int,int);
void windmill(int,int,int);
void sky(int,int);
void filledCircle(int,int,int);
void blade(int,int,int,double,double);

int main(){
  int width = 600;
  int height = 500;
  char cmd;
  int blades = 6;
  double dtheta = 2 * M_PI / blades;
  double theta = 0;
  double omega = .02;
  double domega = .005;
  int blade_length = 125;
  double deltat = 70000;
  
  gfx_open(width, height, "Animation");

  while (true){
    // Create scene
    sky(width, height);
    grass(width, height, 450); 
    windmill(150, 250, 450);
    windmill(450, 250, 450);
    gfx_color(255, 255, 255);
    // Create bldes by incrememnting r,
    // then create same blade incrementing theta to create better fill
    for (int i = 0; i < blades; i++){
      blade(150, 250, blade_length, i * dtheta + theta, dtheta/2);
    }
    for (int i = 0; i < blades; i++){
      blade(450, 250, blade_length, i * dtheta + theta, dtheta/2);
    }
    gfx_color(139, 69, 19);
    // draw a circle over the center of rotation
    filledCircle(150, 250, 35);
    filledCircle(450, 250, 35);

    if (gfx_event_waiting()){
      cmd = gfx_wait();
      if (cmd == 'q') return 0;		// q to quit
      if (cmd == 'f') {			// f to speed up
        // either speeds it up in clockwise or counterclockwise direction
        if (omega >= 0) omega += domega;
        else omega -= domega;
      }
      if (cmd == 's'){
        // Slows down
        if (omega <= domega && omega >= -domega) omega = 0;
        if (omega > domega) omega -= domega;
        if (omega < - domega) omega += domega;
      }
      if (cmd == 'd') omega = -omega;	// Switch direction
      if (cmd == 1){			// Pause animation on mouse click
        cmd = 2;
        while (cmd != 1){
          cmd = gfx_wait();
          if (cmd == 'q') return 0;
        }
      }
    }
    gfx_flush();
    usleep(deltat);
    theta += omega;
    gfx_clear();
  }
}

// Draw the sky 
void sky(int width, int height){
  gfx_color(0,0,255);
  // Draw line every pixel to represent a fill
  for (int i = 0; i < height; i++){
    gfx_line(0, i, width, i);
  }
}

// Draw grass in same manner as sky
void grass(int width, int height, int start){
  gfx_color(0,255,0);
  for (int i = start; i <= height; i++){
    gfx_line(0, i, width, i);
  }
}

// Draw base of windmill
void windmill(int middle, int top, int bottom){
  gfx_color(139, 69, 19);
  for (int i = 0; i < bottom - top; i++){
    gfx_line(middle - 25 - i/3, top + i, middle + 25 + i/3, top + i);
  }
}

// draw circles using vertical lines and equation y = sqrt(r^2 - x^2)
void filledCircle(int x, int y, int r){
  for (int i = 0; i < 2*r; i++){
    gfx_line(x - r + i, y + sqrt(pow(r, 2) - pow(i - r, 2)), x - r + i, y - sqrt(pow(r, 2) - pow(i - r, 2)));
  }
}

// draw blades with lines pointed outwards and tangent lines
void blade(int x, int y, int r, double theta, double a){
  double angle;
  for (int i = 0; i < r; i++){
    angle = theta + a/r * i - a/2;
    if (i > 35)
      // tangents
      gfx_line(x + i * cos(theta - a/2), y + i * sin(theta - a/2), x + i * cos(theta + a/2), y + i * sin(theta + a/2));
    // pointed outwards
    gfx_line(x + 35 * cos(angle), y + 35 * sin(angle), x + r * cos(angle), y + r * sin(angle));
  }
}
