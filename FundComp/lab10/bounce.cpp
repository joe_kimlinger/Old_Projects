// Joe Kimlinger
#include "gfx.h"
#include <unistd.h>
#include <cstdlib>
#include <time.h>
using namespace std;

int main(){
  // Set window dimensions
  int width = 500;
  int height = 400;
  gfx_open(width, height, "Bounce");

  char cmd;
  int deltat = 3000;
  double dx = .5;
  double dy = .5;
  double xpos = width/2;	// Start ball in middle of window
  double ypos = height/2;
  int r = 15;
  srand(time(NULL)); 

  while (true){
    gfx_clear();
    xpos += dx;
    ypos += dy;
    gfx_circle(xpos, ypos, r);

    // Left wall collsision detection
    if (xpos <= r){
      xpos = r;
      dx = -dx;
    }
    // Right wall
    if (xpos >= width - r){
      xpos = width - r;
      dx = -dx;
    }
    // Cieling
    if (ypos <= r){
      ypos = r;
      dy = -dy;
    }
    // Floor
    if (ypos >= height - r){
      ypos = height - r;
      dy = -dy;
    }
    gfx_flush();
    usleep(deltat);

    // If button is pressed check which one
    if (gfx_event_waiting()){
      cmd = gfx_wait();
      if (cmd == 1){
        // Left mouse click sets random x and y velocity between -1 and 1
        xpos = gfx_xpos();
        ypos = gfx_ypos();
        dx = (double)rand()/RAND_MAX * 2.0 - 1;
        dy = (double)rand()/RAND_MAX * 2.0 - 1;   
      }
      else if (cmd == 'q') return 0;	// q to quit
    }
  }
}
