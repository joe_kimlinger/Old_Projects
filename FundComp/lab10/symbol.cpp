// Joe Kimlinger
#include "gfx.h"
#include <math.h>
using namespace std;

int main(){
  char c;  
  bool game = true;
  int r = 13;

  gfx_open(500, 500, "Symbolic Typewriter");
  gfx_clear();

  while (game){
    c = gfx_wait();
    switch (c) {
      case 1:
        gfx_color(0, 0, 255);		// color to blue
        // Display a square
        gfx_line(gfx_xpos() + r, gfx_ypos() + r, gfx_xpos() - r, gfx_ypos() + r);
        gfx_line(gfx_xpos() - r, gfx_ypos() + r, gfx_xpos() - r, gfx_ypos() - r);
        gfx_line(gfx_xpos() - r, gfx_ypos() - r, gfx_xpos() + r, gfx_ypos() - r);
        gfx_line(gfx_xpos() + r, gfx_ypos() - r, gfx_xpos() + r, gfx_ypos() + r);
        break;
      case 't':
        gfx_color(0,255,0);		// Green
        // Display a trianlge
        gfx_line(gfx_xpos() + r, gfx_ypos() + r*cos(1/2), gfx_xpos() - r, gfx_ypos() + r*cos(1/2));
        gfx_line(gfx_xpos() + r, gfx_ypos() + r*cos(1/2), gfx_xpos(), gfx_ypos() - r*cos(1/2));
        gfx_line(gfx_xpos() - r, gfx_ypos() + r*cos(1/2), gfx_xpos(), gfx_ypos() - r*cos(1/2));
        break;
      case 'c':
        gfx_color(255, 255, 255);	// White
        gfx_circle(gfx_xpos(), gfx_ypos(), r);
        break;
      case 'q':
        game = false;
        break; 
    }
    if (c > 50 && c < 58){
      gfx_color(128, 0, 128);
      double dtheta = 2 * M_PI / (c - 48);
      double theta = 0;
      // Go comletely around the unit circle, drawing from previous point to 
      // point incremented by dtheta (polar coordinate stuff)
      while (theta < 2 * M_PI){
        gfx_line(gfx_xpos() + r * cos(theta), gfx_ypos() + r * sin(theta), gfx_xpos() + r * cos(theta + dtheta), gfx_ypos() + r * sin(theta + dtheta));
        theta += dtheta;
      }
    }
  }
}
